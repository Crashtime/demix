#include "demix.h"

using namespace std;


// Default contructor (remember what to put here)
demix::demix () {
	
	fpath = "";		// Input 	file path	
	rpath = "";		// Reference 	file path

	ifilesafe = "";		// Input 	file name
	rfilesafe = "";		// Reference 	file name
	ofilesafe = "";		// Observable 	file name	
	pfilesafe = "";		// Parameter	file name	


}


// THE constructor
demix::demix ( const string & nfilem, const string & rfilem, const string & ofilem, const string & pfilem ) {

	// Seting random seed
	gRandom = new TRandom3( time(NULL) );

	// Setting file name variables
	ifilesafe = nfilem;		// Input 	file name
	rfilesafe = rfilem;		// Reference 	file name
	pfilesafe = pfilem;		// Observable 	file name 
	ofilesafe = ofilem;		// Parameter	file name 

	// Reading input text files
	readFile( nfilem, 0 );		// Reading input 	file name
	readFile( rfilem, 1 );		// Reading reference 	file name
	readFile( ofilem, 2 );		// Reading observable 	file name 
	readFile( pfilem, 3 );		// Reading parameter	file name 

	// Setting kappas and fractions' ready
	kappas.resize( observables.size() );
	fracti.resize( observables.size() );

	// Looping on file names and filling file vectors
	for ( int iFil = 0; iFil < fnames.size(); ++iFil ) ifiles.push_back( new TFile( fnames[ iFil ][1].c_str()) );
	for ( int iRef = 0; iRef < rnames.size(); ++iRef ) rfiles.push_back( new TFile( rnames[ iRef ][1].c_str()) );

	// Getting the observable histograms
	getHists();

	// Prunign the histograms from noisy bins
	getPrunedHists();

	// Gettting histograms in limist if required
	if ( inlims ) getHistsInLims(); 	
	
	// Converting histograms to graphs
	conHists2Grphs();


}


// Auxiliary constructor
demix::demix ( vector< vector< TGraphErrors* > > input, vector< vector< int > > ilims, vector< vector< string > > names, const string & rfilem, vector< vector< string > > & ofilem, const string & pfilem ) {

	fnames = names;
	igrphs = input;
	observables = ofilem;

	readFile( rfilem, 1 );
	readFile( pfilem, 3 );
 
	gRandom = new TRandom3( time(NULL) );

	kappas.resize( observables.size() );
	fracti.resize( observables.size() );

	for ( int iRef = 0; iRef < rnames.size(); ++iRef ) rfiles.push_back( new TFile( rnames[ iRef ][1].c_str()) );

	glims = ilims;
	getHists();
	if ( inlims ) getHistsInLims(); // It's removing values or seting bins to zero
	conHists2Grphs();


}


// Destructor (remember what to put in here
demix::~demix () {

	for ( int iFil = 0; iFil < ifiles.size(); ++iFil ) delete ifiles[ iFil ];
 	for ( int iRef = 0; iRef < rfiles.size(); ++iRef ) delete rfiles[ iRef ];
 	for ( int iObs = 0; iObs <    mgr.size(); ++iObs ) delete    mgr[ iObs ];
	for ( int iObs = 0; iObs <    mgr.size(); ++iObs ) delete    kmg[ iObs ];


}


// Function that reads any of the input files and directs it to the propper function
void demix:: readFile( string filen, int wfile ) {

	// If it is the first file it is the input file
	if ( wfile == 0 ) {

		readDat( filen, 1 );	// Sending input file to proper function


	} else if ( wfile == 1 ) {	// If it is the second file it is the reference file

		readDat( filen, 0 );	// Sending reference file to proper function


	} else if ( wfile == 2 ) {	// If it is the third file it is the observable file

		readObs( filen );	// Sending observable file to proper fucntion


	} else if ( wfile == 3 ) {	// If it is the forth file it is the parameter file

		readPrm( filen );	// Sending parameter file to proper function
		

	} else {			// Sending error message due to unexpected file identifier 

		cout << "[demix::readFile] 		Flag given doen't correspont to expected:\n0: inputf iles\n1: reference files\n2: observables\n3: parameters" << endl;


	}


}


// Function that reads the data files
void demix::readDat( string filen, bool inp  ){

	ifstream file( filen.c_str() ); 	// Creating input file stream to read file

	string line;				// Declaring line variable
	stringstream ss;			// Declaring string stream
	getline( file, line );			// Get first line with the directory of the files
	fpath = line;				// Save the directory of the files

	// Checking if file is open
	if ( file.is_open() ) {

		// Looping on file lines
		while( getline( file, line ) ) {

			vector< string > data = parseString( line, "	" ); 	// Parsing line to extract file name and label
			vector< string > aux;					// Declaring auxiliary variable to push file names and labels to class variable
			ss << fpath << data[1];					// Using string stream to merge data directory with file name
			aux.push_back( data[0] );				// Pushing back label to auxiliary variable
			aux.push_back( ss.str() );				// Pushing back full file location to auxiliary variable
			ss.str( string() );					// Clearing stringstream

			// Checking if it is an input or reference file
			if ( inp ){

				fnames.push_back( aux );				// Pushing back label and name to class variable and informing user of the name of the file
				cout << "[demix::readDat] 		Info: Using input file " << fnames[ fnames.size() - 1 ][1] << endl;


			} else {

				rnames.push_back( aux );				// Pushing back label and name to class variable and informing user of the name of the file
				cout << "[demix::readDat] 		Info: Using reference file " << rnames[ rnames.size() - 1 ][1] << endl;


			}		


		}

	// If the file is not open exit with error message informing which file caused the problem
	} else {

		if ( inp ) cout << "[demix::readDat] 		Error: Inputs' file was not opened. Perhaps you misspelled the name of the file?" << endl;
		else       cout << "[demix::readDat] 		Error: References' file was not opened. Perhaps you misspelled the name of the file?" << endl;


	}	


}


// Function that reads observables
void demix::readObs( string filen ){

	ifstream file( filen.c_str() );	 	// Creating input file stream to read file

	string line;				// Declaring line variable

	// Checking if file is opened
	if ( file.is_open() ) {

		// Looping on file lines
		while( getline( file, line ) ) {

			vector< string > obs = parseString( line, "	" ); 	// Parsing line to extract observable name and label                             					
			vector< string > aux;                                   // Declaring auxiliary variable to push observable names and labels to class variable
			aux.push_back( obs[0] );				// Using string stream back label th back label to auxiliary variable              
			aux.push_back( obs[1] );				// Using string stream back observable name to auxiliary variable
			                        			
			observables.push_back( aux );				// Pushing back auxiliary variable to class variable


		}

	
	// If the file is not open exit with error message
	} else {

		cout << "[demix::readInp] 		Error: Observables' file was not opened. Perhaps you misspelled the name of the file?" << endl;


	}	

}


// Funtion that reads parameter file
void demix::readPrm( string filen){

	ifstream file( filen.c_str() ); 	// Creating input file stream to read file

	string line;				// Declaring line variable

	// Getting parameters values
	if ( file.is_open() ) {

		// Looping on the file lines
		while( getline( file, line ) ) {

			vector< string > param = parseString( line, "	" ); 	// Parsing line to extract parameter name and value   
			
			if ( param[0] == "Method" ){				// If parameter is method of dealing with noisy bins pass to corresponding variable and send message

				meto = stoi( param[1] );	
				cout << "[demix::readPrm] 		Info: Using method " << meto << " to deal with low statistic bins." << endl;


			} else if ( param[0] == "KeepFlow" ) {			// If parameter is keep under and over flow flag pass to corresponding variable and send message

				bovf = stoi( param[1] );
				cout << "[demix::readPrm]		Info: Keep flow fag set to " << bovf << "." << endl;
				

			} else if ( param[0] == "Normalize" ) {			// If parameter is normalize histograms flag pass to corresponding variable and send message

				norm = stoi( param[1] );				
				cout << "[demix::readPrm]		Info: Normalize flag set to " << norm << "." << endl;
				

			} else if ( param[0] == "MinCount" ) {			// If parameter is the minimum count (for method 0) pass to corresponding variable and send message

				mincount = stod( param[1] );					
				cout << "[demix::readPrm]		Info: Minimum bin count set to " << mincount << "." << endl;


			} else if ( param[0] == "MaxError" ) {			// If parameter is the maximum error (for method 1) pass to corresponding variable and send message

				maxerror = stod( param[1] );
				cout << "[demix::readPrm]		Info: Maximum bin error set to " << maxerror << "." << endl;
	

			} else if ( param[0] == "Percentage" ) {		// If parameter is the percentage (for method 2) pass to corresponding variable and send message	

				percent = stod( param[1] );
				cout << "[demix::readPrm]		Info: Percentage of the histogram to keep set to " << percent << "." << endl;


			} else if ( param[0] == "Sigma" ) {			// If parameter is the number of sigma (for method 2) pass to corresponding variable and send message

				hard = stoi( param[1] );
				cout << "[demix::readPrm]		Info: Strength of sigma method set to " << hard << "." << endl;


			} else if ( param[0] == "Topics" ) {			// If parameter is the number of topics pass to corresponding variable and send message
		
				numbtop = stoi( param[1] );
				cout << "[demix::readPrm]		Info: Reconstructing " << numbtop << " topics." << endl;
				

			} else if ( param[0] == "Palette" ) {			// If parameter is the color pallete to use when drawing results pass to corresponding variable and send message

				palette = stoi( param[1] );
				cout << "[demix::readPrm]		Info: Palette was set to" << palette << "." << endl;


			} else if ( param[0] == "InLims" ) {			// If parameter is the in limits flag pass to corresponding variable and send message

				inlims = stoi( param[1] );
				cout << "[demix::readPrm]		Info: In limits flag was set to" << inlims << "." << endl;


			} else if ( param[0] == "Rebin" ) {			// If parameter is the rebin value pass to corresponding variable and send message

				rb = stoi( param[1] );
				cout << "[demix::readPrm]		Info: Rebinning was set to" << inlims << "." << endl;


			} else {						// If the parameter is not recognize send message and exit with 1

				cout << "[demix::readPrm]		Error: parameter " << param[0] << " not identified, exiting." << endl;
				exit(1);

			
			}


		}

	
	// If the file is not open exit with error message
	} else {	

		cout << "[demix::readPrm]		Error: Parameters' file was not opened. Perhaps you misspelled the name of the file?" << endl;


	}	


}


// Function that gets histograms from ROOT files
void demix::getHists () {

	// Looping on files
	for ( int iObs = 0; iObs < observables.size(); ++iObs ) {

		vector< TH1D* > aih;	// Auxiliar histogram vector of histograms per observable

		// Looping on input files
		for ( int iFil = 0; iFil < ifiles.size(); ++iFil ) {

			if 	( getHist( ifiles[ iFil ], observables[ iObs ][1], 0 ) != NULL ) aih.push_back( getHist( ifiles[ iFil ], observables[ iObs ][1], 0 ) );
			else 	cout << "[demix::getHists] 		Error: could not get histogram " << observables[ iObs ][0] << " in input file " << fnames[ iFil ][0] << "." << endl; 
	

		}

		// Normalizing histograms in case asked

		for ( int iHis = 0; iHis < aih.size(); ++iHis ) {

			aih[iHis] -> Rebin( rb );
			if ( norm ) aih[iHis] -> Scale( 1. / aih[iHis] -> Integral() );


		}		

		// Pushing back auxiliary vector to histogram's vector
		ihists.push_back( aih );
		
		vector< TH1D* > arh;	// Auxiliar histogram vector of histograms per observable

		// Looping on reference files
		for ( int iRef = 0; iRef < rfiles.size(); ++iRef ) {

			if 	( getHist( rfiles[ iRef ], observables[ iObs ][1], 0 ) != NULL ) arh.push_back( getHist( rfiles[ iRef ], observables[ iObs ][1], 0 ) );
			else 	cout << "[demix::getHists] 	Error: could not get histogram " << observables[ iObs ][0] << " in reference file " << rnames[ iRef ][0] << "." << endl; 
			

		}

		// Normalizing histograms in case asked and rebin as required
		for ( int iHis = 0; iHis < arh.size(); ++iHis ) {

			arh[iHis] -> Rebin( rb );
			if ( norm ) 		arh[iHis] -> Scale( 1. / arh[iHis] -> Integral() );


		}		

		// Pushing back auxiliary vector to histogram's vector
		rhists.push_back( arh );


	}


}


// Function that get's one histogram from one file
TH1D* demix::getHist( TFile* f, string name, bool full ) { 

	// Gettin list of keys for file and sending error message in case the file has no keys
	TList* l = f -> GetListOfKeys();	
	if ( !l ) { cout << "[demix::getHist]	Error: no keys in file " << f -> GetName() << "." << endl; exit(1); }

	// Declaring list of keys iterator
	TIter n( l );
	
	// Declaring auxiliary variables for the keys and the objects
	TKey* k;
	TObject* o;

	// Looping on keys
	while ( ( k = (TKey*)n() ) ) {

		// Reading the object of the key
		o = k -> ReadObj();
	
		// Parsing name of object
		vector< string > bn = parseString( o -> GetName() , "_" );

		// Checking if object is an histogram 
		if ( strcmp( o -> IsA() -> GetName(), "TH1D" ) == 0 ) {
	
			// Getting histogram
			if( full ) {

				if ( strcmp( o -> GetName(), name.c_str() ) == 0 ) { 

					cout << "[demix::getHist]		Info: using " << o -> GetName() << "." << endl; 

					return (TH1D*) f -> Get( o -> GetName() ); 


				}
		

			} else {

				if ( strcmp( bn[ bn.size() -1 ].c_str(), name.c_str() ) == 0 ) { 

					cout << "[demix::getHist] 		Info: using " << o -> GetName() << "." << endl; 

					return (TH1D*) f -> Get( o -> GetName() ); 


				}


			}


		}


	} 

	// Sending error message in case histogram was not found
	cout << "[demix::getHist] 		Error: histogram " << name << " not found in file " << f -> GetName() << "." << endl; 

	return NULL;

}


// Function that returns histograms in the required limits
void demix::getHistinLims( TH1D* hist, vector< int > lims, bool keepflow ){

	// Declaring under and over flow variables
	double uflow = 0.;
	double oflow = 0.;

	// Looping on histogram bins 
	for ( int iBin = 1; iBin < hist -> GetSize() - 1; ++ iBin ) {

		// If the bins are outside of the limits set them to zero and if asked keep flow bins
		if ( iBin < lims[0] ) {

			if ( keepflow ) uflow += hist -> GetBinContent( iBin ); 
			hist -> SetBinContent( iBin, 0 );


		} else if ( iBin > lims[1]) {

			if ( keepflow ) oflow += hist -> GetBinContent( iBin );
			hist -> SetBinContent( iBin, 0 );
	

		}

	} 

	// Adding flow bins 
	double lowlimc = hist -> GetBinContent( lims[0] );
	double upplimc = hist -> GetBinContent( lims[1] );

	hist -> SetBinContent( lims[0], lowlimc + uflow );
	hist -> SetBinContent( lims[1], upplimc + oflow );


}


// Function to calculate global lims (exclusive)
vector< int > demix::getGlobXLims( const vector< vector< int > > & lims ) {

	// Declaring and defining return variable
	vector< int > res;
	res.push_back(0);
	res.push_back(1e6);

	// Looping on limits per input histograms
	for ( int iHis = 0; iHis < lims.size(); ++iHis ) {

		if ( lims[iHis][0] > res[0] ) res[0] = lims[iHis][0];
		if ( lims[iHis][1] < res[1] ) res[1] = lims[iHis][1];


	}

	// Returning result
	return res;


}
    

// Function that prunes histograms to escape noisy bins
void demix::getPrunedHists () {

	// Looping on observables in histograms object
	for ( int iObs = 0; iObs < observables.size(); ++iObs ) {

		vector< vector< int > > al; 			// Auxiliar limits vector for every observable
		
		// Looping on files in histograms object
		for ( int iFil = 0; iFil < ihists[iObs].size(); ++iFil ) {

			al.push_back( getPrunedHist( ihists[iObs][iFil] ) );	// Pruning histogram of noisy bins

		}

		// Adding global lims per observable and informing user
		glims.push_back( getGlobXLims( al ) );
		cout << "[demix::getPrunedHists]		Info: global limits: 	" << glims[ glims.size() - 1 ][0] << "	" << glims[ glims.size() - 1 ][1] << endl;

		// Pushing back auxiliary vector to limits vector		
		lims.push_back( al );


	}


}


// Function that prunes one histogram to escape noisy bins
vector< int > demix::getPrunedHist( TH1D* hist ) {

	// Waring the user if flow is being kept
	if ( bovf ) cout << "[demix::getPrunedHist] 		Info: Keeping flow." << endl;
	else	    cout << "[demix::getPrunedHist] 		Info: Not keeping flow." << endl;

	// Declaring auxiliary histogram and getting overall integral
	TH1D* ahist = new TH1D( *hist );
	double integral = hist -> Integral();

	// Declaring retrun variable
	vector< int > res;

	// Declaring iteration variables
	int iBin = 1;
	int fBin = hist -> GetSize() - 2;
	
	// Declaring flow variables
	double uflow = 0.; 
	double oflow = 0.; 
 
	// Declaring loop conditions
	bool whileb = 1;
	bool overfb = 1;
	bool undefb = 1;

	// Looping while lopp condition is met
	while ( whileb ) { 

		// Method zero cutting noisy bins by minimum count method
		if ( meto == 0 ) {

			if ( undefb && hist -> GetBinContent( iBin ) > mincount / hist -> Integral() ) {
		
				undefb = 0;




			} else if ( undefb ) {
		
				if ( bovf ) {

					uflow += hist -> GetBinContent( iBin );
					++iBin;


				} else {

					++iBin;


				}


			} 

		
			if ( overfb && hist -> GetBinContent( fBin ) > mincount / hist -> Integral() ) {
		
				overfb = 0; 		


			} else if ( overfb ) {

				if ( bovf ) {

					oflow += hist -> GetBinContent( fBin );
					--fBin;


				} else {

					--fBin;
				
				}


			}

			whileb = overfb || undefb;

		} else if (meto == 1 ) {

			if ( undefb && hist -> GetBinError( iBin ) > maxerror  && hist -> GetBinContent( iBin ) != 0) {
		
				undefb = 0;




			} else if ( undefb ) {
		
				if ( bovf ) {

					uflow += hist -> GetBinContent( iBin );
					++iBin;


				} else {

					++iBin;


				}


			} 

		
			if ( overfb && hist -> GetBinError( fBin ) > maxerror && hist -> GetBinContent( fBin ) != 0 ) {
		
				overfb = 0; 		


			} else if ( overfb ) {

				if ( bovf ) {

					oflow += hist -> GetBinContent( fBin );
					--fBin;


				} else {

					--fBin;
				
				}


			}


			whileb = overfb || undefb;
if (fBin	 == 0 || iBin == hist -> GetSize() - 2 ) exit(0);


		} else if (meto == 2 ) {


			if ( ahist -> Integral() > percent * integral ) { 
		
				if ( ahist -> GetBinContent(iBin) > ahist -> GetBinContent( fBin  ) ) {
		
					if ( bovf ) {
					
						oflow += ahist -> GetBinContent( fBin );
						ahist -> SetBinContent( fBin, 0 );
						--fBin;
		
					} else {
		
						ahist -> SetBinContent( fBin, 0 );
						--fBin;
			
					}
		
				} else {
		
					if( bovf ) {
		
						uflow += ahist -> GetBinContent( iBin );
						ahist -> SetBinContent( iBin, 0 );
						++iBin;
			
					} else {
		
						ahist -> SetBinContent( iBin, 0 );
						++iBin;
			
					}
		
				}
		
		
			} else {

				whileb = 0;


			}


		} else if (meto == 3 ) {



			if ( undefb && hist -> GetBinContent( iBin ) + hard * hist -> GetBinError( iBin ) < hist -> GetBinContent( iBin + 1 ) - hard * hist -> GetBinError( iBin + 1 ) && hist -> GetBinContent( iBin ) != 0 ) {
		
				undefb = 0;




			} else if ( undefb ) {
		
				if ( bovf ) {

					uflow += hist -> GetBinContent( iBin );
					++iBin;


				} else {

					++iBin;


				}


			}

		
			if ( overfb && hist -> GetBinContent( fBin ) + hard * hist -> GetBinError( fBin ) < hist -> GetBinContent( fBin - 1 ) - hard * hist -> GetBinError( fBin - 1 ) && hist -> GetBinContent( fBin ) != 0 ) {
		
				overfb = 0; 		


			} else if ( overfb ) {

				if ( bovf ) {

					oflow += hist -> GetBinContent( fBin );
					--fBin;


				} else {

					--fBin;
				
				}


			}

			whileb = overfb || undefb;


		} else {
		
			cout << "[demix:getPrunedHist]		Error: Selected method is not defined, exiting." << endl;
			exit(1);


		}


	}
	
	double fBinc = hist -> GetBinContent( fBin );
	double iBinc = hist -> GetBinContent( iBin );

	hist -> SetBinContent( iBin , iBinc + uflow );
	hist -> SetBinContent( fBin , fBinc + oflow );

	res.push_back( iBin );
	res.push_back( fBin );

	delete ahist;
	
	return res;


}


// Function that gets histograms in obtained required limits
void demix::getHistsInLims () {

	cout << "[demix::getHIstsInLims]		Getting hists in lims." << endl;

	// Looping on observables
	for ( int iObs = 0; iObs < ihists.size(); ++iObs ) {
	
		// Looping on files
		for ( int iFil = 0; iFil < ihists[iObs].size(); ++iFil ) {

			getHistinLims( ihists[iObs][iFil], glims[iObs], bovf );


		}


	}	


}


// Function that gets graphs in limits
void demix::getGrphsInLims () {

	for ( int iObs = 0; iObs < observables.size(); ++ iObs ) {

		cout << "[demix::getGrphsInLims] 	Observable " << observables[iObs][0] << "." << endl; 

		for ( int iInp = 0; iInp < igrphs[ iObs ].size(); ++iInp ) {

			cout << "[demix::getGrphsInLims] 	Input graph " << iInp << "." << endl; 

			TGraphErrors* aux = new TGraphErrors( glims[ iObs ][1] - glims[ iObs ][0] + 1 );
	
			for ( int iPnt = glims[ iObs ][0]; iPnt <= glims[ iObs ][1]; ++iPnt ) {

				double x,y = 0;
				double ex = igrphs[ iObs ][ iInp ] -> GetErrorX( iPnt );
				double ey = igrphs[ iObs ][ iInp ] -> GetErrorY( iPnt );
				igrphs[ iObs ][ iInp ] -> GetPoint( iPnt, x, y );

				aux -> SetPoint	    ( iPnt - glims[ iObs ][0],  x,  y );
				aux -> SetPointError( iPnt - glims[ iObs ][0], ex, ey );


			}

			igrphs[ iObs ][ iInp ] = aux;

		}
		
		for ( int iRef = 0; iRef < rgrphs[ iObs ].size(); ++iRef ) {

			TGraphErrors* aux = new TGraphErrors( glims[ iObs ][1] - glims[ iObs ][0] + 1 );
	
			for ( int iPnt = glims[ iObs ][0]; iPnt <= glims[ iObs ][1]; ++iPnt ) {

				double x,y = 0;
				double ex = rgrphs[ iObs ][ iRef ] -> GetErrorX( iPnt );
				double ey = rgrphs[ iObs ][ iRef ] -> GetErrorY( iPnt );
				rgrphs[ iObs ][ iRef ] -> GetPoint( iPnt, x, y );

				aux -> SetPoint	    ( iPnt - glims[ iObs ][0],  x,  y );
				aux -> SetPointError( iPnt - glims[ iObs ][0], ex, ey );


			}

			rgrphs[ iObs ][ iRef ] = aux;


		}


	}


}


// Function that Converts histograms to graphs
void demix::conHists2Grphs() {

	// Looping on observables
	for ( int iObs = 0; iObs < observables.size(); ++iObs ) {

		// Declaring auxiliar graph vectors
		vector< TGraphErrors* > iagrphs;
		vector< TGraphErrors* > ragrphs;

		// Looping on input files
		for ( int iFil = 0; iFil < ihists[ iObs ].size(); ++ iFil ) {

			// Declaring auxiliar graph
			TGraphErrors* agrph = new TGraphErrors( glims[ iObs ][1] - glims[ iObs ][0] + 1 );		

			// Looping on histogram bins
			for ( int iBin = glims[ iObs ][0]; iBin <= glims[ iObs ][1]; ++iBin ) {

				agrph -> SetPoint(      iBin - glims[ iObs ][0], ihists[ iObs ][ iFil ] -> GetBinCenter( iBin ) + ihists[ iObs ][ iFil ] -> GetBinWidth( iBin ) / 2.,     ihists[ iObs ][ iFil ] -> GetBinContent( iBin ) );
				agrph -> SetPointError( iBin - glims[ iObs ][0], ihists[ iObs ][ iFil ] -> GetBinWidth( iBin ) / 2., ihists[ iObs ][ iFil ] -> GetBinError(   iBin ) );


			} 	// Finished looping on bins

			// Pushing back graph for this observable and this file
			iagrphs.push_back( agrph );


		} 	// Finished looping on input files

		// Looping on reference files
		for ( int iRef = 0; iRef < rhists[ iObs ].size(); ++ iRef ) {

			// Declaring auxiliar graph
			TGraphErrors* agrph = new TGraphErrors( glims[ iObs ][1] - glims[ iObs ][0] + 1 );		

			// Looping on histogram bins
			for ( int iBin = glims[ iObs ][0]; iBin <= glims[ iObs ][1]; ++iBin ) {

				agrph -> SetPoint(      iBin - glims[ iObs ][0], rhists[ iObs ][ iRef ] -> GetBinCenter( iBin ) + rhists[ iObs ][ iRef ] -> GetBinWidth( iBin ) / 2.,     rhists[ iObs ][ iRef ] -> GetBinContent( iBin ) );
				agrph -> SetPointError( iBin - glims[ iObs ][0], rhists[ iObs ][ iRef ] -> GetBinWidth( iBin ) / 2., rhists[ iObs ][ iRef ] -> GetBinError(   iBin ) );


			} 	// Finished looping on bins

			// Pushing back graph for this observable and this file
			ragrphs.push_back( agrph );


		} 	// Finished looping on files

		// Pushing back graph vector for this observable
		igrphs.push_back( iagrphs ); 
		rgrphs.push_back( ragrphs ); 

		// Pushing back kappa histogram
		stringstream ss;
		vector< TGraphErrors* > k12aux;
		vector< TGraphErrors* > k21aux;
		k12aux.push_back( new TGraphErrors( glims[ iObs ][1] - glims[ iObs ][0] + 1 ) );
		k12aux.push_back( new TGraphErrors( glims[ iObs ][1] - glims[ iObs ][0] + 1 ) );
		k12aux.push_back( new TGraphErrors( glims[ iObs ][1] - glims[ iObs ][0] + 1 ) );
		k21aux.push_back( new TGraphErrors( glims[ iObs ][1] - glims[ iObs ][0] + 1 ) );
		k21aux.push_back( new TGraphErrors( glims[ iObs ][1] - glims[ iObs ][0] + 1 ) );
		k21aux.push_back( new TGraphErrors( glims[ iObs ][1] - glims[ iObs ][0] + 1 ) );
		ss << "Kappa12_1sig_" << observables[ iObs ][1];
		k12aux[0] -> SetName( ss.str().c_str() );
		ss.str( string() );
		ss << "Kappa12_2sig_" << observables[ iObs ][1];
		k12aux[1] -> SetName( ss.str().c_str() );
		ss.str( string() );
		ss << "Kappa12_3sig_" << observables[ iObs ][1];
		k12aux[2] -> SetName( ss.str().c_str() );
		ss.str( string() );
		ss << "Kappa21_1sig_" << observables[ iObs ][1];
		k21aux[0] -> SetName( ss.str().c_str() );
		ss.str( string() );
		ss << "Kappa21_2sig_" << observables[ iObs ][1];
		k21aux[1] -> SetName( ss.str().c_str() );
		ss.str( string() );
		ss << "Kappa21_3sig_" << observables[ iObs ][1];
		k21aux[2] -> SetName( ss.str().c_str() );
		ss.str( string() );
		vector< vector< TGraphErrors* > > kaux;
		kaux.push_back( k12aux );
		kaux.push_back( k21aux );
		kgraphs.push_back( kaux );


	}	// Finished looping on observables


}


// Function that calculates reducibility factor, it's statistical error, and anchor bin
vector< vector< double > > demix::getKappas( TGraphErrors* S1, TGraphErrors* S2, int Obs ) {

	//Declaring return variables
	vector< double > k12( 3, 0 );
	vector< double > k21( 3, 0 );
	vector< vector< double > > k;

	// Auxiliar variables
	double count1 = 0.   	;	// Point y value for S1
	double count2 = 0.   	;	// Point y value for S2
	double conte1 = 0.   	;	// Point y error value for S1
	double conte2 = 0.   	;	// Point y error value for S2
	double redc12 = 1.   	;	// Reducibility factor from S1 to S2 check variable
	double redc21 = 1.   	;	// Reducibility factor from S2 to S1 check variable
	double redv12 = 0.99 	;	// Reducibility factor from S1 to S2 variable
	double redv21 = 0.99 	;	// Reducibility factor from S2 to S1 variable
	double rdek12 = 1.   	;	// Reducibility factor from S1 to S2 error variable
	double rdek21 = 1.   	;	// Reducibility factor from S2 to S1 error variable
	double rede12 = 0.   	;	// Reducibility factor from S1 to S2 error variable
	double rede21 = 0.   	;	// Reducibility factor from S2 to S1 error variable
	double anch12 = 0    	;	// Anchor point from S1 to S2 variable
	double anch21 = 0    	;	// Anchor point from S2 to S1 variable

	// Geting TGraphErrors arrays
	Double_t*  x  = S1 -> GetX();
	Double_t*  ex = S1 -> GetEX();
	Double_t*  y1 = S1 -> GetY();
	Double_t*  y2 = S2 -> GetY();
	Double_t* ey1 = S1 -> GetEY();
	Double_t* ey2 = S2 -> GetEY();

	// Looping on the bins of the graphs
	//for ( int iPnt = 0; iPnt < S1 -> GetN(); ++iPnt ) {
	for ( int iPnt = glims[Obs][0]; iPnt <= glims[Obs][1]; ++iPnt ) {

		// Geting graphs' content at this bin
		count1 =  y1[ iPnt - glims[Obs][0] ];
		count2 =  y2[ iPnt - glims[Obs][0] ];
		conte1 = ey1[ iPnt - glims[Obs][0] ];
		conte2 = ey2[ iPnt - glims[Obs][0] ];

		redc12 = count1 / count2;
		redc21 = count2 / count1;
		rede12 = sqrt( ( conte1 * conte1 ) / ( count2 * count2 ) + ( conte2 * conte2 * count1 * count1 ) / ( count2 * count2 * count2 * count2 ) ); 
		rede21 = sqrt( ( conte2 * conte2 ) / ( count1 * count1 ) + ( conte1 * conte1 * count2 * count2 ) / ( count1 * count1 * count1 * count1 ) ); 

		// Making reducibility factor plots
		kgraphs[ Obs ][0][0] -> SetPoint( iPnt - glims[Obs][0], x[iPnt - glims[Obs][0]] , redc12 );
		kgraphs[ Obs ][0][0] -> SetPointError( iPnt - glims[Obs][0], ex[iPnt - glims[Obs][0]] , rede12 );
		kgraphs[ Obs ][1][0] -> SetPoint( iPnt - glims[Obs][0], x[iPnt - glims[Obs][0]] , redc21 );
		kgraphs[ Obs ][1][0] -> SetPointError( iPnt - glims[Obs][0], ex[iPnt - glims[Obs][0]] , rede21 );
		kgraphs[ Obs ][0][1] -> SetPoint( iPnt - glims[Obs][0], x[iPnt - glims[Obs][0]] , redc12 );
		kgraphs[ Obs ][0][1] -> SetPointError( iPnt - glims[Obs][0], ex[iPnt - glims[Obs][0]] , 2 * rede12 );
		kgraphs[ Obs ][1][1] -> SetPoint( iPnt - glims[Obs][0], x[iPnt - glims[Obs][0]] , redc21 );
		kgraphs[ Obs ][1][1] -> SetPointError( iPnt - glims[Obs][0], ex[iPnt - glims[Obs][0]] , 2 * rede21 );
		kgraphs[ Obs ][0][2] -> SetPoint( iPnt - glims[Obs][0], x[iPnt - glims[Obs][0]] , redc12 );
		kgraphs[ Obs ][0][2] -> SetPointError( iPnt - glims[Obs][0], ex[iPnt - glims[Obs][0]] , 3 * rede12 );
		kgraphs[ Obs ][1][2] -> SetPoint( iPnt - glims[Obs][0], x[iPnt - glims[Obs][0]] , redc21 );
		kgraphs[ Obs ][1][2] -> SetPointError( iPnt - glims[Obs][0], ex[iPnt - glims[Obs][0]] , 3 * rede21 );


		// Checking if the check kappa from S1 to S2 is smaller than the previously saved value and if it's compatible; if so updating kappa variable
		if ( redc12 < redv12 ) { 

			// Checking kappa compatibility with positive definite histogram nature
			bool cred12 = 1;

			// Looping on points
			for ( int jPnt = glims[Obs][0]; jPnt <= glims[Obs][1]; ++jPnt ) {

				double ccount1 =  y1[ jPnt - glims[Obs][0] ];
				double ccount2 =  y2[ jPnt - glims[Obs][0] ];
				double cconte1 = ey1[ jPnt - glims[Obs][0] ];
				double cconte2 = ey2[ jPnt - glims[Obs][0] ];

				double tpcount = ( ccount1 - redc12 * ccount2 ) / ( 1 - redc12 );
				double tperror = sqrt( ( cconte1 * cconte1 ) / ( ( 1 - redc12 ) * ( 1 - redc12 ) ) + ( redc12 * redc12 * cconte2 * cconte2 ) / ( ( 1 - redc12 ) * ( 1 - redc12 ) ) + ( ( ( ccount1 - ccount2 ) * ( ccount1 - ccount2 ) ) * rede12 * rede12 ) / ( ( 1 - redc12 ) * ( 1 - redc12 ) * ( 1 - redc12 ) * ( 1 - redc12 ) ) );

				if ( fabs( tpcount ) > 1e-4 &&  tpcount < 0 ) cred12 =0;
			}

			if ( cred12 ) {
		
				rdek12 = rede12;
				redv12 = redc12; 
				anch12 = x[ iPnt - glims[Obs][0] ]; 


			}


		}

		// Checking if the check kappa from S2 to S1 is smaller than the previously saved value and if it's compatible; if so updating kappa variable
		if ( redc21 < redv21 ) { 

			// Checking kappa compatibility with positive definite histogram nature
			bool cred21 = 1;
			for ( int jPnt = glims[Obs][0]; jPnt <= glims[Obs][1]; ++jPnt ) {

				double ccount1 = y1[ jPnt - glims[Obs][0] ];
				double ccount2 = y2[ jPnt - glims[Obs][0] ];
				double cconte1 = ey1[ jPnt - glims[Obs][0] ];
				double cconte2 = ey2[ jPnt - glims[Obs][0] ];

				double tpcount = ( ccount2 - redc21 * ccount1 ) / ( 1 - redc21 );
				double tperror = sqrt( ( cconte2 * cconte2 ) / ( ( 1 - redc21 ) * ( 1 - redc21 ) ) + ( redc21 * redc21 * cconte1 * cconte1 ) / ( ( 1 - redc21 ) * ( 1 - redc21 ) ) + ( ( ( ccount2 - ccount1 ) * ( ccount2 - ccount1 ) ) * rede21 * rede21 ) / ( ( 1 - redc21 ) * ( 1 - redc21 ) * ( 1 - redc21 ) * ( 1 - redc21 ) ) );

				if ( fabs( tpcount ) > 1e-4 &&  tpcount < 0 ) cred21 =0;


			}

			if ( cred21 ) {

				rdek21 = rede21;
				redv21 = redc21; 
				anch21 = x[ iPnt - glims[Obs][0] ];


			}


		}

		// Reseting count variables to zero
		count1 = 0;
		count2 = 0;
		

	}	//Finished loopig on bins

	// Filling return variables
	if ( redv12 != .99 ) {

		k12[0] = redv12;
		k12[1] = rdek12;
		k12[2] = anch12;

	
	} else {

		k12[0] =  0;
		k12[1] =  0;
		k12[2] = -1;


	}

	if ( redv21 != .99 ) {

		k21[0] = redv21;
		k21[1] = rdek21;
		k21[2] = anch21;


	} else {

		k21[0] =  0;
		k21[1] =  0;
		k21[2] = -1;


	}

	k.push_back( k12 );
	k.push_back( k21 );
		
	// Returning result
	return k;


}




// Function that calculates topic fraction from reducibility factors
vector< vector< double > > demix::getFractions( vector< vector< double > > k ) {

	vector< vector< double > > res;

	vector< double > frac1;
	vector< double > frac2;

	frac1.push_back( ( 1 - k[0][0]) / ( 1 - k[0][0] * k[1][0] ) );
	frac1.push_back( 1 - frac1[0] );
	frac1.push_back(  sqrt(  ( ( ( 1 - k[0][0] ) * ( 1 - k[0][0] ) * k[0][0] * k[0][0] ) / ( ( 1 - k[0][0] * k[1][0] ) * ( 1 - k[0][0] * k[1][0] ) * ( 1 - k[0][0] * k[1][0] ) * ( 1 - k[0][0] * k[1][0] ) ) ) * k[1][1] * k[1][1] + ( ( ( k[1][0] - 1 ) * ( k[1][0] - 1 ) ) / (  ( 1 - k[0][0] * k[1][0] ) * ( 1 - k[0][0] * k[1][0] ) * ( 1 - k[0][0] * k[1][0] ) * ( 1 - k[0][0] * k[1][0] ) ) ) * k[0][1] * k[0][1]  )  );
	frac2.push_back( k[1][0] * frac1[0]  );
	frac2.push_back( 1 - frac2[0] );
	frac2.push_back(  sqrt( frac1[0] * frac1[0] * k[1][1] * k[1][1] + k[1][0] * k[1][0] * frac1[2] * frac1[2] )  );


	res.push_back( frac1 );
	res.push_back( frac2 );

	return res;


}

// Getting residue distributions
vector< TGraphErrors* > demix::getResidues( TGraphErrors* S1, TGraphErrors* S2, int Obs ) {

	// Defining return variable
	TGraphErrors* R12 = new TGraphErrors( S1 -> GetN() );
	TGraphErrors* R21 = new TGraphErrors( S1 -> GetN() );
	vector< TGraphErrors* > residues;

	// Geting kappas
	kappas[Obs] = getKappas( S1, S2, Obs );
	fracti[Obs] = getFractions( kappas[Obs] );	

	// Geting TGraphErrors arrays
	Double_t*  x1 = S1 -> GetX();
	Double_t*  y1 = S1 -> GetY();
	Double_t*  y2 = S2 -> GetY();
	Double_t* ex1 = S1 -> GetEX();
	Double_t* ey1 = S1 -> GetEY();
	Double_t* ey2 = S2 -> GetEY();

	// Looping through points
	for ( int iPnt = 0; iPnt < S1 -> GetN() ; ++iPnt ) { 

		// Geting graph content for this point
		double  n1 =  y1[ iPnt ];	
		double  n2 =  y2[ iPnt ];	
		double en1 = ey1[ iPnt ];	
		double en2 = ey2[ iPnt ];	
		double   x =  x1[ iPnt ];	
		double  ex = ex1[ iPnt ];	

		// Geting topic content for this bin
		double c1 = ( n1 - kappas[Obs][0][0] * n2 ) / ( 1 - kappas[Obs][0][0] );
		double c2 = ( n2 - kappas[Obs][1][0] * n1 ) / ( 1 - kappas[Obs][1][0] );

		// Geting topic statistical error for this bin
		double ec1 = sqrt( ( en1 * en1 ) / ( ( 1 - kappas[Obs][0][0] ) * ( 1 - kappas[Obs][0][0] ) ) + ( kappas[Obs][0][0] * kappas[Obs][0][0] * en2 * en2 ) / ( ( 1 - kappas[Obs][0][0] ) * ( 1 - kappas[Obs][0][0] ) ) + ( ( ( n1 - n2 ) * ( n1 - n2 ) ) * kappas[Obs][0][1] * kappas[Obs][0][1] ) / ( ( 1 - kappas[Obs][0][0] ) * ( 1 - kappas[Obs][0][0] ) * ( 1 - kappas[Obs][0][0] ) * ( 1 - kappas[Obs][0][0] ) ) );
		double ec2 = sqrt( ( en2 * en2 ) / ( ( 1 - kappas[Obs][1][0] ) * ( 1 - kappas[Obs][1][0] ) ) + ( kappas[Obs][1][0] * kappas[Obs][1][0] * en1 * en1 ) / ( ( 1 - kappas[Obs][1][0] ) * ( 1 - kappas[Obs][1][0] ) ) + ( ( ( n2 - n1 ) * ( n2 - n1 ) ) * kappas[Obs][1][1] * kappas[Obs][1][1] ) / ( ( 1 - kappas[Obs][1][0] ) * ( 1 - kappas[Obs][1][0] ) * ( 1 - kappas[Obs][1][0] ) * ( 1 - kappas[Obs][1][0] ) ) );

		// Seting points and errors to topics' objects
		R12 -> SetPoint	    ( iPnt,  x,  c1 );
		R21 -> SetPoint	    ( iPnt,  x,  c2 );
		R12 -> SetPointError( iPnt, ex, ec1 );
		R21 -> SetPointError( iPnt, ex, ec2 );
		

	}
	
	if ( kappas[Obs][0][0] == .99 ) R12 = S1;
	if ( kappas[Obs][0][1] == .99 ) R21 = S2;

	// Filling return variable
	residues.push_back( R12 ); 	
	residues.push_back( R21 ); 	

	// Returning result
	return residues;


}


// Function that calcualtes average distribution of distributions
TGraphErrors* demix::getAver( const vector< TGraphErrors* > graphs ) {

	TGraphErrors* aver;
	if ( graphs.size() != 0 ) aver = new TGraphErrors( graphs[0] -> GetN() );	
	else 			  cout << "[demix::getConv] 		Seems like the graphs vector for this function is empty. Exiting with one" << endl;

	// Declaring graphs' arrays
	vector< Double_t* >  y;
	vector< Double_t* >  x;
	vector< Double_t* > ey;
	vector< Double_t* > ex;
	vector< double    >  a;

	// Geting graphs arrays and checking if all TGraphErrors have the same number of points
	int size = graphs[0] -> GetN();		// Reference varible for number of points
	y. push_back( graphs[0] -> GetY()  );
	x. push_back( graphs[0] -> GetX()  );
	ey.push_back( graphs[0] -> GetEY() );
	ex.push_back( graphs[0] -> GetEX() );

	for ( int iGra = 1; iGra < graphs.size(); ++iGra ) {

		// Checking size of this graph
		if ( graphs[ iGra ] -> GetN() != size ) {

			// Sending error message
			cout << "[demix::getConv] 		Error: Not all graphs have the same size. returning empty object."<< endl;
			return aver;


		}

		// Pushing back this graph's arrays
		y. push_back( graphs[ iGra ] -> GetY()  );
		x. push_back( graphs[ iGra ] -> GetX()  );
		ey.push_back( graphs[ iGra ] -> GetEY() );
		ex.push_back( graphs[ iGra ] -> GetEX() );
		


	}

	// Looping on graphs' points
	for ( int iPnt = 0; iPnt < size; ++iPnt ) {

		double  ny = 0.;
		double  nx = 0.;
		double eny = 0.;
		double enx = 0.;
	
		// Looping on graphs
		for ( int iGra = 0; iGra < graphs.size(); ++iGra ) {
	
			// Building conv point
			ny +=  y[ iGra ][ iPnt ] / graphs.size();
			eny += ( ey[ iGra ][ iPnt ] ) / graphs.size();
	

		}
		

		// Filling in result
		aver -> SetPoint(      iPnt,  x[0][ iPnt ],  ny );
		aver -> SetPointError( iPnt, ex[0][ iPnt ], eny );


	}
	
	// Returning result
	return aver;
	

}


// Function that calculates uniformely distributed element in convex hull of set of distributions
TGraphErrors* demix::getConv( const vector< TGraphErrors* > graphs ) {

	// Declaring return variable
	TGraphErrors* conv;
	if ( graphs.size() != 0 ) conv = new TGraphErrors( graphs[0] -> GetN() );	
	else 			  cout << "[demix::getConv] 		Seems like the graphs vector for this function is empty. Exiting with one" << endl;

	// Declaring graphs' arrays
	vector< Double_t* >  y;
	vector< Double_t* >  x;
	vector< Double_t* > ey;
	vector< Double_t* > ex;
	vector< double    >  a;

	// Geting random fraction
	bool gotRand = 0;
	while ( !gotRand ) {

		double sum = 0.;

		for ( int i = 0; i < graphs.size() - 1; ++i ) {

			a.push_back( gRandom -> Uniform() );
			sum += a[i];			


		} 


		if ( sum < 1 ){
	
			gotRand = 1;
			a.push_back( 1 - sum ); 


		} else {

			a.clear();
		

		}


	}

	cout << "Random Fractions:	" << endl;

	for ( int i = 0; i < a.size(); ++i ) cout << a[i] << endl;

	// Geting graphs arrays and checking if all TGraphErrors have the same number of points
	int size = graphs[0] -> GetN();		// Reference varible for number of points
	y. push_back( graphs[0] -> GetY()  );
	x. push_back( graphs[0] -> GetX()  );
	ey.push_back( graphs[0] -> GetEY() );
	ex.push_back( graphs[0] -> GetEX() );
	for ( int iGra = 1; iGra < graphs.size(); ++iGra ) {

		// Checking size of this graph
		if ( graphs[ iGra ] -> GetN() != size ) {

			// Sending error message
			cout << "[demix::getConv] 		Error: Not all graphs have the same size. returning empty object."<< endl;
			return conv;


		}

		// Pushing back this graph's arrays
		y. push_back( graphs[ iGra ] -> GetY()  );
		x. push_back( graphs[ iGra ] -> GetX()  );
		ey.push_back( graphs[ iGra ] -> GetEY() );
		ex.push_back( graphs[ iGra ] -> GetEX() );
		


	}

	// Looping on graphs' points
	for ( int iPnt = 0; iPnt < size; ++iPnt ) {

		double  ny = 0.;
		double  nx = 0.;
		double eny = 0.;
		double enx = 0.;
	
		// Looping on graphs
		for ( int iGra = 0; iGra < graphs.size(); ++iGra ) {
	
			// Building conv point
			ny +=  y[ iGra ][ iPnt ] * a[ iGra ];
			eny += ( ey[ iGra ][ iPnt ] * ey[ iGra ][ iPnt ] ) * ( a[ iGra ] * a[ iGra ] );


		}
		
		// Squaring the error squared
		eny = sqrt( eny );

		// Filling in result
		conv -> SetPoint(      iPnt,  x[0][ iPnt ],  ny );
		conv -> SetPointError( iPnt, ex[0][ iPnt ], eny );


	}
	
	// Returning result
	return conv;
	

}


// Function that calculates specific element in convex hull of set of distributions
TGraphErrors* demix::getConv( const vector< TGraphErrors* > graphs, vector< double > & rfracs ) {

	cout << "[demix::getConv]	Geting conv." << endl;
	// Declaring return variable
	TGraphErrors* conv;
	if ( graphs.size() != 0 ) conv = new TGraphErrors( graphs[0] -> GetN() );	
	else 			  cout << "[demix::getConv] 		Seems like the graphs vector for this function is empty. Exiting with one" << endl;

	// Declaring graphs' arrays
	vector< Double_t* >  y;
	vector< Double_t* >  x;
	vector< Double_t* > ey;
	vector< Double_t* > ex;

	// Geting random fraction
	bool gotRand = 0;
	while ( !gotRand ) {

		double sum = 0.;

		for ( int i = 0; i < graphs.size() - 1; ++i ) {

			rfracs.push_back( gRandom -> Uniform() );
			sum += rfracs[i];			


		} 

		if ( sum < 1 ){
	
			gotRand = 1;
			rfracs.push_back( 1 - sum ); 


		} else {

			rfracs.clear();
		

		}

	}

	cout << "Random Fractions:	" << endl;

	for ( int i = 0; i < rfracs.size(); ++i ) cout << rfracs[i] << endl;

	// Geting graphs arrays and checking if all TGraphErrors have the same number of points
	int size = graphs[0] -> GetN();		// Reference varible for number of points
	y. push_back( graphs[0] -> GetY()  );
	x. push_back( graphs[0] -> GetX()  );
	ey.push_back( graphs[0] -> GetEY() );
	ex.push_back( graphs[0] -> GetEX() );

	for ( int iGra = 1; iGra < graphs.size(); ++iGra ) {

		if ( graphs[ iGra ] -> GetN() != size ) {

			// Sending error message
			cout << "[demix::getConv] 		Error: Not all graphs have the same size. returning empty object."<< endl;
			return conv;


		}

		// Pushing back this graph's arrays
		y. push_back( graphs[ iGra ] -> GetY()  );
		x. push_back( graphs[ iGra ] -> GetX()  );
		ey.push_back( graphs[ iGra ] -> GetEY() );
		ex.push_back( graphs[ iGra ] -> GetEX() );
		


	}

	// Looping on graphs' points
	for ( int iPnt = 0; iPnt < size; ++iPnt ) {

		double  ny = 0.;
		double  nx = 0.;
		double eny = 0.;
		double enx = 0.;
	
		// Looping on graphs
		for ( int iGra = 0; iGra < graphs.size(); ++iGra ) {
	
			// Building conv point
			ny +=  y[ iGra ][ iPnt ] * rfracs[ iGra ];
			eny += ( ey[ iGra ][ iPnt ] * ey[ iGra ][ iPnt ] ) * ( rfracs[ iGra ] * rfracs[ iGra ] );


		}
		
		// Square rooting the error squared
		eny = sqrt( eny );

		// Filling in result
		conv -> SetPoint(      iPnt,  x[0][ iPnt ],  ny );
		conv -> SetPointError( iPnt, ex[0][ iPnt ], eny );


	}
	
	// Returning result
	return conv;
	

}


// FaceTest: checking if none of the kappas are zero for a given set of distributions and observable
bool demix::FaceTest( const vector< TGraphErrors* > R, int Obs ) {

	// Auxiliar kappas vector
	vector< double > k;

	// Looping over graphs
	for ( int iGr1 = 0; iGr1 < R.size(); ++iGr1 ) {

		// Looping over other graphs
		for ( int iGr2 = iGr1 + 1; iGr2 < R.size(); ++iGr2 ) {

			// Geting kappas
			vector< vector < double > > thiskappas = getKappas( R[ iGr1 ], R[ iGr2 ], Obs );

			// Pushing back kappas to auxiliar object
			k.push_back( thiskappas[0][0] );
			k.push_back( thiskappas[1][0] );

		}    


	}

	// Checking for zero valued kappas
	for ( int iKap = 0; iKap < k.size(); ++iKap ) {

		// If any kappa is zero face test is failed
		if ( k[ iKap ] < 1e-6 || k[ iKap ] > 0.98 ) return 0; 


	}

	return 1;


}


// Scale TGraphErrors
TGraphErrors* demix::TGraphErrorsScale( const TGraphErrors* S, const double & s ){

	// Declaring return variable
	TGraphErrors* res = new TGraphErrors( S -> GetN() );

	// Geting input graph arrays
	Double_t*  y = S -> GetY();
	Double_t*  x = S -> GetX();
	Double_t* ey = S -> GetEY();
	Double_t* ex = S -> GetEX();

	// Looping on points
	for ( int iPnt = 0; iPnt < S -> GetN(); ++iPnt ) {

		res -> SetPoint(      iPnt,  x[ iPnt ],  y[ iPnt ] * s );
		res -> SetPointError( iPnt, ex[ iPnt ], ey[ iPnt ] * s );


	}

	// Returning result
	return res;


}


// Sum TGraphErrors
TGraphErrors* demix::TGraphErrorsSum( const TGraphErrors* S1, const TGraphErrors* S2 ){

	// Declaring return object
	TGraphErrors* res = new TGraphErrors( S1 -> GetN() );
	
	// Checking if graphs have the same size
	if ( S1 -> GetN() != S2 -> GetN() ) { 

		cout << "[demix::TGraphErrorsScale] 	Error: Given grpahs have diferen number of points. Returning empty graph." << endl;
		return res;


	}

	// Geting graphs arrays
	Double_t*  y1 = S1 -> GetY();
	Double_t*  y2 = S2 -> GetY();
	Double_t*  x1 = S1 -> GetX();
	Double_t* ey1 = S1 -> GetEY();
	Double_t* ey2 = S2 -> GetEY();
	Double_t* ex1 = S1 -> GetEX();
	
	// Looping on points
	for ( int iPnt = 0; iPnt < S1 -> GetN(); ++iPnt ) {

		// Geting point error
		double error = ey1[ iPnt ] + ey2[ iPnt];

		// Setting result point
		res -> SetPoint(      iPnt,  x1[ iPnt ], y1[ iPnt ] + y2[ iPnt ] );
		res -> SetPointError( iPnt, ex1[ iPnt ], error );


	}	// Finished looping on points
	
	// Returning result
	return res;


}

// Actual demix algorithm (square) to test multiple possibilities
int demix::SquareDemix( const vector< TGraphErrors* > S, int Obs, vector< double > &fracs, int tol ){

	// Defining return object
	vector< TGraphErrors* > topics;

	// Checking size of histograms object to proceed accordingly
	if ( S.size() == 2 ) {

		// Geting topics
		topics = getResidues( S[0], S[1], Obs );

	} else {

		// Defining auxiliar variables 
		vector< TGraphErrors* > R;		// Vector of residues to be calculated
		vector< TGraphErrors* > Qs;		// Vector of residues to be calculated
		vector< TGraphErrors* > SA;		// Vector of residues to be calculated
		for ( int iSam = 1; iSam < S.size(); ++iSam ) SA.push_back( S[ iSam ] );
		TGraphErrors* Q = getConv( SA, fracs );	// Uniformely distributed element in conv(S1, ..., Sk)
		double n = 1.;				// Variable to define proportion of given S in residue calculation for FaceTest
		bool T = 0;				// FaceTest check variable (1 one FaceTest passes)

		// Looping while FaceTest fails
		while( !T ) {
	
			n += 1;				// Iterating proportion of S's for Face test (as it goes up the algorithm tests with lower proportions
			for ( int i =0; i < R.size(); ++i ) delete R[i];
			R.clear();
			// Looping over samples: Geting residues of a weighted sum of the conv(...) and some S to S1 (nothing special about S1)
			for ( int iSam = 1; iSam < S.size() ; ++iSam ) {


				// Auxiliar objects that will be scaled
				TGraphErrors* A;
				TGraphErrors* AS = new TGraphErrors( *S[iSam] 	       );
				TGraphErrors* AQ = new TGraphErrors( *Q	     	       );

				// Scaling auxiliar GraphErrors
				AS = TGraphErrorsScale( AS,    	 1.   / n ); 
				AQ = TGraphErrorsScale( AQ, ( n - 1. ) / n );  

				// Suming auxiliar GraphErrors
				A = TGraphErrorsSum( AS, AQ );
				// Geting Residue
				R.push_back( getResidues( A , S[0], Obs )[0] );				
				
				delete AS;
				delete AQ;
				delete A;

			} 	// Finished looping over samples

			// Performing face test			
			T = FaceTest( R, Obs );

			if ( n == tol ) return 0;  


		} 	// FaceTest must be approved

		// Demixing Residues to obtain final results
		Qs = SquareDemix( R, Obs );
		for ( int i =0; i < R.size(); ++i ) delete R[i];
		R.clear();
		if ( Qs[0] == NULL ) return 0;

		// Auxiliar for final Q
		TGraphErrors* Qf = getAver( S );
		TGraphErrors* Aux;
	

		// Geting final Q from the residue of each (iteratively) to a uniform mixture of the calculated ones
		for ( int iRes = 0; iRes < Qs.size(); ++iRes ) {

			Aux = getResidues( Qf, Qs[iRes], Obs )[0];
			delete Qf;
			Qf = new TGraphErrors( (*Aux) );
			delete Aux;

		} 
	
		// Adding final Q to vectro and setting it to topics variable
		Qs.push_back( Qf );
		topics = Qs;


	}	// Algorithm's done

	// Returning results
	vector < vector < double > > sigdiffref = getSigmaDiff( topics, rgrphs[ Obs ], Obs, 1 );
	vector < vector < double > > sigdifftop = getSigmaDiff( topics, rgrphs[ Obs ], Obs, 0 );
	vector < double > err = getGoodErrMeas( topics, Obs );

	vector< double > sigref(topics.size(), 10e10);
	vector< double > sigtop(topics.size(), 10e10);

	for ( int iTop = 0; iTop < sigdifftop.size(); ++iTop ) {

		for ( int iRef = 0; iRef < sigdifftop[ iTop ].size(); ++iRef ) {

			if ( sigdifftop[ iTop ][ iRef ] < sigtop[ iTop ] ) sigtop[ iTop ] = sigdifftop[ iTop ][ iRef ];
			if ( sigdiffref[ iTop ][ iRef ] < sigref[ iTop ] ) sigref[ iTop ] = sigdiffref[ iTop ][ iRef ];
		
						


		}
	

	}

	double sigmtop = 0;
	double sigmref = 0;

	sort( sigtop.begin(), sigtop.end() );
	sort( sigref.begin(), sigref.end() );

	for ( int i = 0; i < sigtop.size(); ++i ) {

		if ( i < 2 ) sigmtop += sigtop[i] / 2;
		sigmref += sigref[i] / sigref.size();


 	}

	if ( err[0] < .05 ) cout << "GEM: " << err[0] << "	" << err[1] << "	" << sigmref << "	" << sigmtop << endl; 
	cout << sigmref << endl;
	
	if ( sigmref < 1.5 && sigmtop < 1 && err[0] < .05 && err[1] < .05 ) {

		return 6;


	} else if( sigmref < 2 && sigmtop < 1 && err[0] < .05 && err[1] < .05 ){

		return 5;


	} else if ( sigmref < 3 && sigmtop < 1 && err[0] < .05 && err[1] < .05 ) {

		return 4;


	} else if( sigmref < 3 && sigmtop < 1 && err[0] < .1 && err[1] < .1 ){

		return 3;


	} else if( sigmref < 5 && sigmtop < 1  ){

		return 2;


	} else {
	
		return 1;


	}


}


// Actual demix algorithm (square)
vector< TGraphErrors* > demix::SquareDemix( const vector< TGraphErrors* > S, int Obs ){

	// Defining return object
	vector< TGraphErrors* > topics;

	// Checking size of histograms object to proceed accordingly
	if ( S.size() == 2 ) {

		// Geting topics
		topics = getResidues( S[0], S[1], Obs );


	} else {

		// Defining auxiliar variables 
		vector< TGraphErrors* > R;		// Vector of residues to be calculated
		vector< TGraphErrors* > Qs;		// 
		vector< TGraphErrors* > SA;		// 
		for ( int iSam = 1; iSam < S.size(); ++iSam ) SA.push_back( S[ iSam ] );
		TGraphErrors* Q = getConv( SA );	// Uniformely distributed element in conv(S1, ..., Sk)
//		TGraphErrors* Q = getAver( SA );	
		double n = 1.;				// Variable to define proportion of given S in residue calculation for FaceTest
		bool T = 0;				// FaceTest check variable (1 one FaceTest passes)

		// Looping while FaceTest fails
		while( !T ) {
			n += 1;				// Iterating proportion of S's for Face test (as it goes up the algorithm tests with lower proportions
			for ( int i = 0; i < R.size(); ++i ) delete R[ i ];

			R.clear();
			// Looping over samples: Geting residues of a weighted sum of the conv(...) and some S to S1 (nothing special about S1)
			for ( int iSam = 1; iSam < S.size() ; ++iSam ) {


				// Auxiliar objects that will be scaled
				TGraphErrors* A;
				TGraphErrors* AS = new TGraphErrors( *S[iSam] 	       );
				TGraphErrors* AQ = new TGraphErrors( *Q	     	       );

				// Scaling auxiliar GraphErrors
				AS = TGraphErrorsScale( AS,    	 1.   / n ); 
				AQ = TGraphErrorsScale( AQ, ( n - 1. ) / n );  

				A = TGraphErrorsSum( AS, AQ );

				// Geting Residue
				R.push_back( getResidues( A , S[0], Obs )[0] );				
				
				delete AS;
				delete AQ;
				delete A;

			} 	// Finished looping over samples

			T = FaceTest( R, Obs );
			if ( n == 1000 ) return( vector< TGraphErrors * >(1, NULL) );


		} 	// FaceTest must be approved

		// Demixing Residues to obtain final results
		Qs = SquareDemix( R, Obs );

		for ( int i =0; i < R.size(); ++i ) delete R[i];
		R.clear();
		// Auxiliar for final Q
		TGraphErrors* Qf = getAver( S );
		TGraphErrors* Aux;

		// Geting final Q from the residue of each (iteratively) to a uniform mixture of the calculated ones
		for ( int iRes = 0; iRes < Qs.size(); ++iRes ) {

			Aux = getResidues( Qf, Qs[iRes], Obs )[0];
			delete Qf;
			Qf = new TGraphErrors( *Aux ); 
			delete Aux;


		} 
	
		// Adding final Q to vectro and setting it to topics variable
		Qs.push_back( Qf );
		topics = Qs;


	}	// Algorithm's done

	// Returning results
	return topics;


}


// Non squared demix algorithm ( here just Demix )
void demix::Demix() {

	cout << "[demix::Demix]		Running Demix." << endl;


	// Looping on observables 
	for ( int iObs = 0; iObs < observables.size(); ++iObs ) {

		// Checking if we have square case or not
		if ( numbtop == igrphs[ iObs ].size() ) {

			cout << "[demix::Demix]		Running Square Demix." << endl;
			topics.push_back( SquareDemix( igrphs[ iObs ], iObs ) );
		

		} else {

			cout << "[demix::Demix]		Running non Square Demix." << endl;
			int n = (int) igrphs[ iObs ].size() / numbtop ; 	// Number of graphs to be considered for the first set of auxiliar graphs 
			int r = igrphs[ iObs ].size() - n;				// Remainder for last graph to be considered
			

			// Auxiliar graph object
			vector< TGraphErrors* >	Q;

			int iSet = 0;

			// Looping on the number of topics
			for ( int iTop = 0; iTop < numbtop; ++iTop ) {

				Q.push_back( getConv( igrphs[ iObs ] ) );
				

			} 	// Ending loop to get the conv's


			topics.push_back( SquareDemix( Q, iObs ) );

		}


	}	// Ending loop on observables
	

}


vector< vector< int > >	demix::getGlims() {

	return glims;

}


vector< vector< TGraphErrors* > >	demix::getTopics() {

	return topics;


}


vector< vector< vector< double > > >	demix::getAfrcs() {

	return fracti;


}

vector < double >	demix::getGoodErrMeas( vector< TGraphErrors* > top,  int iObs ) {

	vector< double > res;

	for ( int iTop = 0; iTop < top.size(); ++ iTop ) {
		
		double obstopres = 0;	
		Double_t* ey = top[iTop] -> GetEY();

		for ( int iPnt = 0; iPnt < top[iTop] -> GetN(); ++iPnt ) {

			obstopres += ey[iPnt];


		}

		//obstopres /= top[iTop] -> GetN();
		obstopres /= top[iTop] -> GetMean(2) * top[iTop] -> GetN();
//cout << "Error measure final:" << obstopres << endl;

		res.push_back( obstopres );			


	}
	
	return res;


}

vector< vector < double > >	demix::getGoodErrMeas() {

	vector< vector< double > > res;

	for ( int iObs = 0; iObs < topics.size(); ++iObs ) {

		vector< double > obsres;

		for ( int iTop = 0; iTop < topics[ iObs ].size(); ++ iTop ) {
			
			double obstopres = 0;	
			Double_t* ey = topics[iObs][iTop] -> GetEY();

			for ( int iPnt = 0; iPnt < topics[iObs][iTop] -> GetN(); ++iPnt ) {

				obstopres += ey[iPnt];

	
			}
	
			obstopres /= topics[iObs][iTop] -> GetMean(2) * topics[iObs][iTop] -> GetN();

			obsres.push_back( obstopres );			

		
		}
		
		res.push_back( obsres );


	}

	return res;


}


vector< vector< vector< double > > > 	demix::getChi2T() {

	vector< vector< vector< double > > > res;

	for ( int iObs = 0; iObs < topics.size(); ++iObs ) {

		vector< vector< double > > obsres;

		for ( int iTop = 0; iTop < topics[ iObs ].size(); ++ iTop ) {

			vector< double > topres( rgrphs[ iObs ].size(), 0 );
		
			double*  cy = topics[ iObs ][ iTop ] -> GetY(); 
			double* ecy = topics[ iObs ][ iTop ] -> GetEY(); 

			for ( int iPnt = 0; iPnt < topics[ iObs ][ iTop ] -> GetN(); ++iPnt ) {

				for ( int iRef = 0; iRef < rgrphs[ iObs ].size(); ++iRef ) {

					double  x,  y = 0;
					double ey = rgrphs[ iObs ][ iRef ] -> GetErrorY( iPnt );

					rgrphs[ iObs ][ iRef ] -> GetPoint( iPnt, x, y );

					topres[ iRef ] += ( ( y - cy[ iPnt ] ) * ( y - cy[ iPnt ] ) ) / ( topics[ iObs ][ iTop ] -> GetN() * sqrt( ey * ey * ey * ey + ecy[ iPnt ] * ecy[ iPnt ] * ecy[ iPnt ] * ecy[ iPnt ] ) );


				}
			

			}		
			
			obsres.push_back( topres );


		}

		res.push_back( obsres );


	}

	return res;

}

vector< vector< vector< double > > > 	demix::getSigmaDiff( bool r ) {

	vector< vector< vector< double > > > res;

	for ( int iObs = 0; iObs < topics.size(); ++ iObs ) {

		cout << "[demix::getSigmaDiff]		Calculating average sigma for observable " << observables[ iObs ][0] << " with " << rgrphs[ iObs ].size() << " reference." << endl;

		vector< vector< double > > obsres;

		for ( int iTop = 0; iTop < topics[ iObs ].size(); ++ iTop ) {

			cout << "[demix::getSigmaDiff]		Calculating average sigma for topic " << iTop << "." << endl;

			vector< double > topres( rgrphs[ iObs ].size(), 0 );
		
			double*  cy = topics[ iObs ][ iTop ] -> GetY(); 
			double* ecy = topics[ iObs ][ iTop ] -> GetEY(); 

			for ( int iPnt = 0; iPnt < topics[ iObs ][ iTop ] -> GetN(); ++iPnt ) {

				for ( int iRef = 0; iRef < rgrphs[ iObs ].size(); ++iRef ) {

					double  x,  y = 0;
					double ey = rgrphs[ iObs ][ iRef ] -> GetErrorY( iPnt );

					rgrphs[ iObs ][ iRef ] -> GetPoint( iPnt, x, y );

					if ( r ) topres[ iRef ] += abs( cy[ iPnt ] - y ) / ( 2 * ( ey  ) * topics[iObs][ iTop ] -> GetN() ); 
					else     topres[ iRef ] += abs( cy[ iPnt ] - y ) / ( 2 * ( ecy[ iPnt ]  ) * topics[iObs][ iTop ] -> GetN() );

				}
			

			}		
			
			obsres.push_back( topres );


		}

		res.push_back( obsres );


	}

	return res;

}

vector< vector< double > > 	demix::getSigmaDiff( vector< TGraphErrors* > top, vector< TGraphErrors* > ref, int iObs, bool r ) {

	vector< vector< double > > res;

	for ( int iTop = 0; iTop < top.size(); ++ iTop ) {

		cout << "[demix::getSigmaDiff]	Topic " << iTop << "." << endl;

		vector< double > topres( ref.size(), 0 );
	
		double*  cy = top[ iTop ] -> GetY(); 
		double* ecy = top[ iTop ] -> GetEY(); 

		for ( int iPnt = 0; iPnt < top[ iTop ] -> GetN(); ++iPnt ) {

			for ( int iRef = 0; iRef < ref.size(); ++iRef ) {

				double  x,  y = 0;
				double ey = ref[ iRef ] -> GetErrorY( iPnt );

				ref[ iRef ] -> GetPoint( iPnt, x, y );

				if ( r ) topres[ iRef ] += abs( cy[ iPnt ] - y ) / ( 2 * ( ey  ) * top[ iTop ] -> GetN() ); 
				else     topres[ iRef ] += abs( cy[ iPnt ] - y ) / ( 2 * ( ecy[ iPnt ]  ) * top[ iTop ] -> GetN() );


			}
		

		}		
		
		res.push_back( topres );


	}


	return res;

}


// Draw topics
void demix::drawTopics( bool inp, bool ref, bool wait, string obs, string name ) {

	// Setting style (better put this in it's own function on myROOT lybrary or something)
	gROOT -> SetStyle("ATLAS");
	gStyle -> SetPalette( palette );

	// Condition to draw which observable or all
	if ( strcmp( obs.c_str(), "all" ) == 0 ) {

		// Looping on observables
		for ( int iObs = 0; iObs < observables.size(); ++iObs ) {
	
			// Declaring objects to be drawn 
			mgr.push_back( new TMultiGraph() );				// Multigraph object to do automatic color pallete and common axis
			TLegend* l = new TLegend( .68, .65, .93, .89, "", "NDC");		// Legend
			TLegend* s = new TLegend( .6,  .5, .99, .69, "", "NDC");

			// Looping on topic objects
			for ( int iTop = 0; iTop < numbtop; ++iTop ) {

				// No marker's please, I wanna see the error bars (yum bars)
				TGraphErrors* g = (TGraphErrors* )topics[ iObs ][ iTop ] -> Clone();
cout << iTop << endl;
				g -> SetMarkerStyle( 0 );
									
				// Adding to Multigraph
				mgr[ iObs ] -> Add( g );
				
				// Adding entry to legend
				stringstream ss;	
				ss << name << " topic " << iTop + 1;
				g -> SetTitle( ss.str().c_str() );
				g -> SetName( ss.str().c_str() );
				g -> SetFillStyle(0);
				l -> AddEntry( g, ss.str().c_str(), "lep" );


			}	// Finished looping on topics
		
			// Adding input graphs if requested
			if ( inp ) {

				for ( int iGra = 0; iGra < igrphs[ iObs ].size(); ++iGra ) {

					mgr[ iObs ] -> Add( igrphs[ iObs ][ iGra ]);
					igrphs[ iObs ][ iGra ] -> SetTitle( fnames[ iGra ][0].c_str() );
					igrphs[ iObs ][ iGra ] -> SetFillStyle(0);
					igrphs[ iObs ][ iGra ] -> SetMarkerStyle( 0 );

					l -> AddEntry( igrphs[iObs][iGra], fnames[ iGra ][0].c_str(), "l" );

				}


			}

			// Adding input graphs if requested
			if ( ref ) {

				for ( int iRef = 0; iRef < rgrphs[ iObs ].size(); ++iRef ) {

					mgr[ iObs ] -> Add( rgrphs[ iObs ][ iRef ]);
					rgrphs[ iObs ][ iRef ] -> SetTitle( rnames[ iRef ][0].c_str() );
					rgrphs[ iObs ][ iRef ] -> SetFillStyle(0);
					rgrphs[ iObs ][ iRef ] -> SetMarkerStyle( 0 );

					l -> AddEntry( rgrphs[iObs][iRef], rnames[ iRef ][0].c_str(), "l" );

				}


			}
	
			// Seting title of TGraph to give some usefull information			
			stringstream ss;
			if ( meto == 0 ) ss << hard << "#sigma";
			else 		 ss << percent * 100 << "%";
			ss.str( string() );

			// Drawing multigrah object
			TCanvas * c = new TCanvas("top","", 1200, 500);
			c -> Divide( 2, 1, 0.000001, 0.005, 0 );
			c -> cd (1);
			mgr[ iObs ] -> Draw( "APL PLC" );
			mgr[ iObs ] -> GetXaxis() -> SetTitle( observables[ iObs ][0].c_str() );
			mgr[ iObs ] -> GetYaxis() -> SetTitle( "Probability density" );
			mgr[ iObs ] -> GetYaxis() -> SetMaxDigits(4);
			mgr[ iObs ] -> GetYaxis() -> SetNdivisions(4,5,0);
			c -> Modified();			

			// Building legend
			s -> SetTextSize( .024);
			l-> SetFillStyle(0);
			s-> SetFillStyle(0);
			ss.str( string() );
			ss << "a_{zd} = " << kappas[iObs][0][2];
			ss.str( string() );
			ss << "a_{dz} = " << kappas[iObs][1][2];
			ss.str( string() );
			ss << "K_{zd} = " << kappas[iObs][0][0] << " +- " << kappas[iObs][0][1];
			ss.str( string() );
			s -> AddEntry( (TObject*)0, ss.str().c_str(), "" );
			ss.str( string() );
			ss << "f_{k=1}^{i=1} = " << setprecision(3) << fracti[iObs][0][0] << " +- " << fracti[iObs][0][2];
			s -> AddEntry( (TObject*)0, ss.str().c_str(), "" );
			ss.str( string() );
			ss << "f_{k=1}^{i=2} = " << setprecision(3) << fracti[iObs][1][0] << " +- " << fracti[iObs][1][2];
			s -> AddEntry( (TObject*)0, ss.str().c_str(), "" );
			ss.str( string() );
			s -> SetTextSize( .03 );
			l -> SetBorderSize(0);
			s -> SetBorderSize(0);

			l -> Draw( "same" );
			s -> Draw( "same" );

			c -> cd (2);
			gPad->SetFrameBorderMode(0);
			gPad->SetBorderMode(0);
			gPad->SetBorderSize(0);

			kgraphs[ iObs ][0][0] -> SetLineColor( kRed );
			kgraphs[ iObs ][0][0] -> SetFillColorAlpha( kRed , .3 );
			kgraphs[ iObs ][0][0] -> SetMarkerStyle( 0 );

			kgraphs[ iObs ][1][0] -> SetLineColor( kBlue );
			kgraphs[ iObs ][1][0] -> SetFillColorAlpha( kBlue, .3 );
			kgraphs[ iObs ][1][0] -> SetMarkerStyle( 0 );

			kgraphs[ iObs ][0][1] -> SetFillColorAlpha( kRed, .2 );
			kgraphs[ iObs ][0][1] -> SetMarkerStyle( 0 );

			kgraphs[ iObs ][1][1] -> SetFillColorAlpha( kBlue, .2 );
			kgraphs[ iObs ][1][1] -> SetMarkerStyle( 0 );

			kgraphs[ iObs ][0][2] -> SetFillColorAlpha( kRed , .1 );
			kgraphs[ iObs ][0][2] -> SetMarkerStyle( 0 );

			kgraphs[ iObs ][1][2] -> SetFillColorAlpha( kBlue, .1 );
			kgraphs[ iObs ][1][2] -> SetMarkerStyle( 0 );

			kmg.push_back( new TMultiGraph() );
			ss << "#kappa evolution;" << observables[ iObs ][0] << ";#kappa;"; 
			kmg[ iObs ] -> SetTitle( ss.str().c_str()  );
			kmg[ iObs ] -> Add( kgraphs[ iObs ][0][0], "L" );
			kmg[ iObs ] -> Add( kgraphs[ iObs ][1][0], "L" );
			kmg[ iObs ] -> Add( kgraphs[ iObs ][0][0], "3" );
			kmg[ iObs ] -> Add( kgraphs[ iObs ][1][0], "3" );
			kmg[ iObs ] -> Add( kgraphs[ iObs ][0][1], "3" );
			kmg[ iObs ] -> Add( kgraphs[ iObs ][1][1], "3" );
			kmg[ iObs ] -> Add( kgraphs[ iObs ][0][2], "3" );
			kmg[ iObs ] -> Add( kgraphs[ iObs ][1][2], "3" );
			kmg[ iObs ] -> SetMaximum( 1 );
			kmg[ iObs ] -> Draw( "A" );

			kgraphs[ iObs ][1][0] -> SetMarkerStyle( 0 );

			TGraph * p12 = new TGraph( 1 );
			TGraph * p21 = new TGraph( 1 );
			p12 -> SetName( "a12" );
			p21 -> SetName( "a21" );
			p12 -> SetPoint( 0, kappas[iObs][0][2], kappas[iObs][0][0] );
			p21 -> SetPoint( 0, kappas[iObs][1][2], kappas[iObs][1][0] );
			p12 -> SetMarkerColor( kGreen + 2 );
			p21 -> SetMarkerColor( kGreen + 2 );
			p12 -> SetMarkerStyle( 24 );
			p21 -> SetMarkerStyle( 24 );
			p12 -> SetMarkerSize( 3 );
			p21 -> SetMarkerSize( 3 );

			c -> cd (2);
			p12 -> Draw( "P" );
			p21 -> Draw( "P" );
			
			c -> Modified();			
			
			gPad -> WaitPrimitive();
			// Printing to file
			ss.str( string() );	
			ss << name << "_" << observables[ iObs ][1] << "_topics.pdf";
			c -> Print( ss.str().c_str() );
			ss.str( string() );
			ss << observables[ iObs ][1] << "_topics.C";
			c -> Print( ss.str().c_str() );


		}	// Finished looping on observables


	} else {

		

	}

}


void demix::testConv( int n , string A, string B, string C ){

	TernaryPlot* t1 = new TernaryPlot( 0 );
	t1 -> SetATitle(fnames[0][0]);
	t1 -> SetBTitle(fnames[1][0]);
	t1 -> SetCTitle(fnames[2][0]);
	TCanvas * c1 = new TCanvas( "c1" );
	t1 -> Draw("");

	TLegend* l = new TLegend( .1, .7, .59, .99, "", "NDC");
	
	bool cl1 = 1;
	bool cl2 = 1;
	bool cl3 = 1;
	bool cl4 = 1;
	bool cl5 = 1;
	bool cl6 = 1;
	
	TGraph gr1(0);	
	TGraph gr2(0);	
	TGraph gr3(0);	
	TGraph gr4(0);	
	TGraph gr5(0);	
	TGraph gr6(0);	

	gr1.SetName( "kRed" );
	gr2.SetName( "kOrange" );
	gr3.SetName( "kYellow" );
	gr4.SetName( "kGreen" );
	gr5.SetName( "kBlue" );
	gr6.SetName( "kBlack" );

	gr1.SetLineColor( kRed );
	gr2.SetLineColor( kOrange );
	gr3.SetLineColor( kYellow );
	gr4.SetLineColor( kGreen );
	gr5.SetLineColor( kBlue );
	gr6.SetLineColor( kBlack );

	l -> AddEntry( &gr1, "<#sigma>_{ref} < 1.5 && <#sigma>_{top} < 1 && E_{1} < .05 && E_{2} < .05", "l"  );
	l -> AddEntry( &gr2, "<#sigma>_{ref} < 2    && <#sigma>_{top} < 1 && E_{1} < .05 && E_{2} < .05", "l" );
	l -> AddEntry( &gr3, "<#sigma>_{ref} < 3    && <#sigma>_{top} < 1 && E_{1} < .05 && E_{2} < .05", "l" );
	l -> AddEntry( &gr4, "<#sigma>_{ref} < 3    && <#sigma>_{top} < 1 && E_{1} <  .1 && E_{2} <  .1", "l" );
	l -> AddEntry( &gr5, "<#sigma>_{ref} < 5    && <#sigma>_{top} < 1", "l" 			      );
	l -> AddEntry( &gr6, "all other options", "l" 							      );

	l -> SetFillStyle( 0 );
	l -> SetBorderSize( 0 );


	for ( int iObs = 0; iObs < igrphs.size();++iObs ) {

		cout << "[demix::TestConv]	testing observable " << observables[ iObs ][0] << "." << endl;


		vector< vector< TGraphErrors* > > ABaux;
		vector< TGraphErrors* > AB;
		AB.push_back( igrphs[ iObs ][0] );
		AB.push_back( igrphs[ iObs ][1] );

		vector< vector < string > > ABnam;
		vector< string > ABaux1;
		vector< string > ABaux2;
		ABaux1.push_back(fnames[0][0]);
		ABaux1.push_back("");
		ABaux2.push_back(fnames[1][0]);
		ABaux2.push_back("");
		ABnam.push_back(ABaux1);
		ABnam.push_back(ABaux2);
		ABaux.push_back(AB);

		vector< vector < string > > obs;
		obs.push_back( observables[ iObs ] );

		demix demAB(ABaux,getGlims(),ABnam, rfilesafe,obs, pfilesafe );
		demAB.Demix();
		vector< TGraphErrors* > topAB = demAB.getTopics()[0];
		vector < vector < double > > sigdiffrefAB = demAB.getSigmaDiff( 1 )[0];
		vector < vector < double > > sigdifftopAB = demAB.getSigmaDiff( 0 )[0];
		vector < double > errAB = demAB.getGoodErrMeas( topAB, iObs );
		
		stringstream ss;
		ss << "AB_" << observables[ iObs ][1];
		demAB.drawTopics( 1, 1, 0, "all", ss.str() );
		ss.str( string() );

		vector< double > sigrefAB(2, 10e10);
		vector< double > sigtopAB(2, 10e10);

		for ( int iTop = 0; iTop < sigdifftopAB.size(); ++iTop ) {

			for ( int iRef = 0; iRef < sigdifftopAB[ iTop ].size(); ++iRef ) {

				if ( sigdifftopAB[ iTop ][ iRef ] < sigtopAB[ iTop ] ) sigtopAB[ iTop ] = sigdifftopAB[ iTop ][ iRef ];
				if ( sigdiffrefAB[ iTop ][ iRef ] < sigrefAB[ iTop ] ) sigrefAB[ iTop ] = sigdiffrefAB[ iTop ][ iRef ];
			
							

			}
	

		}

		double sigmtopAB = ( sigtopAB[0] + sigtopAB[1] ) / 2;
		double sigmrefAB = ( sigrefAB[0] + sigrefAB[1] ) / 2;

		TGraph * gAB = new TGraph( 2 ); 
		gAB -> SetName( "gAB" );

		if ( sigmrefAB < 1.5 && sigmtopAB < 1 && errAB[0] < .05 && errAB[1] < .05 ) {

			gAB -> SetLineColorAlpha( kRed, 1 );


		} else if ( sigmrefAB < 2 && sigmtopAB < 1 && errAB[0] < .05 && errAB[1] < .05 ) {

			gAB -> SetLineColorAlpha( kOrange, 1 );


		} else if ( sigmrefAB < 3 && sigmtopAB < 1 && errAB[0] < .05 && errAB[1] < .05 ) {

			gAB -> SetLineColorAlpha( kYellow, 1 );


		} else if( sigmrefAB < 3 && sigmtopAB < 1 && errAB[0] < .1 && errAB[1] < .1 ){

			gAB -> SetLineColorAlpha( kGreen, 1 );


		} else if( sigmrefAB < 5 && sigmtopAB < 1 ){

			gAB -> SetLineColorAlpha( kBlue, 1 );



		} else {
		
			gAB -> SetLineColorAlpha( kBlack, 1 );


		}

		gAB -> SetPoint( 0,  0 + 1 * .5 - .003, 1 * sqrt( 3 ) / 2 - .007 );
		gAB -> SetPoint( 1,  1 + 0 * .5 - .01, 0 * sqrt( 3 ) / 2 + .005 );
		
		gAB -> SetLineWidth( 3 );

		vector< vector< TGraphErrors* > > ACaux;
		vector< TGraphErrors* > AC;
		AC.push_back( igrphs[ iObs ][0] );
		AC.push_back( igrphs[ iObs ][2] );

		vector< vector < string > > ACnam;
		vector< string > ACaux1;
		vector< string > ACaux2;
		ACaux1.push_back(fnames[0][0]);
		ACaux1.push_back("");
		ACaux2.push_back(fnames[2][0]);
		ACaux2.push_back("");
		ACnam.push_back(ACaux1);
		ACnam.push_back(ACaux2);
		ACaux.push_back(AC);

		demix demAC(ACaux,getGlims(),ACnam, rfilesafe,obs, pfilesafe );
		demAC.Demix();
		vector< TGraphErrors* > topAC = demAC.getTopics()[0];
		vector < vector < double > > sigdiffrefAC = demAC.getSigmaDiff( 1 )[0];
		vector < vector < double > > sigdifftopAC = demAC.getSigmaDiff( 0 )[0];
		vector < double > errAC = demAC.getGoodErrMeas( topAC, iObs );
		ss << "AC_" << observables[ iObs ][1];
		demAC.drawTopics( 1, 1, 0, "all" ,ss.str());
		ss.str( string() );

		vector< double > sigrefAC(2, 10e10);
		vector< double > sigtopAC(2, 10e10);

		for ( int iTop = 0; iTop < sigdifftopAC.size(); ++iTop ) {

			for ( int iRef = 0; iRef < sigdifftopAC[ iTop ].size(); ++iRef ) {

				if ( sigdifftopAC[ iTop ][ iRef ] < sigtopAC[ iTop ] ) sigtopAC[ iTop ] = sigdifftopAC[ iTop ][ iRef ];
				if ( sigdiffrefAC[ iTop ][ iRef ] < sigrefAC[ iTop ] ) sigrefAC[ iTop ] = sigdiffrefAC[ iTop ][ iRef ];
			

			}
	

		}

		double sigmtopAC = ( sigtopAC[0] + sigtopAC[1] ) / 2;
		double sigmrefAC = ( sigrefAC[0] + sigrefAC[1] ) / 2;

		TGraph * gAC = new TGraph( 2 ); 
		gAC -> SetName( "gAC" );

		if ( sigmrefAC < 1.5 && sigmtopAC < 1 && errAC[0] < .05 && errAC[1] < .05 ) {

			gAC -> SetLineColorAlpha( kRed, 1 );


		} else if ( sigmrefAC < 2 && sigmtopAC < 1 && errAC[0] < .05 && errAC[1] < .05 ) {

			gAC -> SetLineColorAlpha( kOrange, 1 );


		} else if ( sigmrefAC < 3 && sigmtopAC < 1 && errAC[0] < .05 && errAC[1] < .05 ) {

			gAC -> SetLineColorAlpha( kYellow, 1 );


		} else if( sigmrefAC < 3 && sigmtopAC < 1 && errAC[0] < .1 && errAC[1] < .1 ){

			gAC -> SetLineColorAlpha( kGreen, 1 );


		} else if( sigmrefAC < 5 && sigmtopAC < 1 ){

			gAC -> SetLineColorAlpha( kBlue, 1 );


		} else {
		
			gAC -> SetLineColorAlpha( kBlack, 1 );


		}

		gAC -> SetPoint( 0,  0 + 1 * .5 + .003, 1 * sqrt( 3 ) / 2 - .007 );
		gAC -> SetPoint( 1,  0  + 0 * .5 + .01 , 0 * sqrt( 3 ) / 2 + .005 );
		
		gAC -> SetLineWidth( 3 );

		vector< vector< TGraphErrors* > > BCaux;
		vector< TGraphErrors* > BC;
		BC.push_back( igrphs[ iObs ][1] );
		BC.push_back( igrphs[ iObs ][2] );

		vector< vector < string > > BCnam;
		vector< string > BCaux1;
		vector< string > BCaux2;
		BCaux1.push_back(fnames[1][0]);
		BCaux1.push_back("");
		BCaux2.push_back(fnames[2][0]);
		BCaux2.push_back("");
		BCnam.push_back(BCaux1);
		BCnam.push_back(BCaux2);
		BCaux.push_back(BC);

		demix demBC(BCaux,getGlims(),BCnam, rfilesafe,obs, pfilesafe );
		demBC.Demix();
		vector< TGraphErrors* > topBC = demBC.getTopics()[0];
		vector < vector < double > > sigdiffrefBC = demBC.getSigmaDiff( 1 )[0];
		vector < vector < double > > sigdifftopBC = demBC.getSigmaDiff( 0 )[0];
		vector < double > errBC = demBC.getGoodErrMeas( topBC, iObs );
		ss << "BC_" << observables[ iObs ][1];
		demBC.drawTopics( 1, 1, 0, "all" ,ss.str());
		ss.str( string() );

		vector< double > sigrefBC(2, 10e10);
		vector< double > sigtopBC(2, 10e10);

		for ( int iTop = 0; iTop < sigdifftopBC.size(); ++iTop ) {

			for ( int iRef = 0; iRef < sigdifftopBC[ iTop ].size(); ++iRef ) {

				if ( sigdifftopBC[ iTop ][ iRef ] < sigtopBC[ iTop ] ) sigtopBC[ iTop ] = sigdifftopBC[ iTop ][ iRef ];
				if ( sigdiffrefBC[ iTop ][ iRef ] < sigrefBC[ iTop ] ) sigrefBC[ iTop ] = sigdiffrefBC[ iTop ][ iRef ];

			}
	

		}

		double sigmtopBC = ( sigtopBC[0] + sigtopBC[1] ) / 2;
		double sigmrefBC = ( sigrefBC[0] + sigrefBC[1] ) / 2;

		TGraph * gBC = new TGraph( 2 ); 
		gBC -> SetName( "gBC" );

		if ( sigmrefBC < 1.5 && sigmtopBC < 1 && errBC[0] < .05 && errBC[1] < .05 ) {

			gBC -> SetLineColorAlpha( kRed, 1 );


		} else if ( sigmrefBC < 2 && sigmtopBC < 1 && errBC[0] < .05 && errBC[1] < .05 ) {

			gBC -> SetLineColorAlpha( kOrange, 1 );


		} else if ( sigmrefBC < 3 && sigmtopBC < 1 && errBC[0] < .05 && errBC[1] < .05 ) {

			gBC -> SetLineColorAlpha( kYellow, 1 );


		} else if( sigmrefBC < 3 && sigmtopBC < 1 && errBC[0] < .1 && errBC[1] < .1 ){

			gBC -> SetLineColorAlpha( kGreen, 1 );


		} else if( sigmrefBC < 5 && sigmtopBC < 1 ){

			gBC -> SetLineColorAlpha( kBlue, 1 );


		} else {
		
			gBC -> SetLineColorAlpha( kBlack, 1 );


		}

		gBC -> SetPoint( 0,  0 + 0 * .5 + .01, 0 * sqrt( 3 ) / 2 + .007 );
		gBC -> SetPoint( 1,  1 + 0 * .5 - .01, 0 * sqrt( 3 ) / 2 + .007 );
		
		gBC -> SetLineWidth( 3 );

		vector< vector< TGraphErrors* > > aversAaux;
		vector< TGraphErrors* > aversA;
		vector< TGraphErrors* > auxA1;
		vector< TGraphErrors* > auxA2;
		auxA1.push_back( igrphs[ iObs ][0] );
		auxA1.push_back( igrphs[ iObs ][1] );
		auxA2.push_back( igrphs[ iObs ][0] );
		auxA2.push_back( igrphs[ iObs ][2] );
		aversA.push_back( getAver( auxA1 ) );
		aversA.push_back( getAver( auxA2 ) );

		vector< vector < string > > nam;
		vector< string > aux1;
		vector< string > aux2;
		aux1.push_back("aver1");
		aux1.push_back("");
		aux2.push_back("aver2");
		aux2.push_back("");
		nam.push_back(aux1);
		nam.push_back(aux2);
		aversAaux.push_back(aversA);

		demix demA(aversAaux,getGlims(),nam, rfilesafe,obs, pfilesafe );
		demA.Demix();
		vector< TGraphErrors* > top = demA.getTopics()[0];
		vector < vector < double > > sigdiffrefA = demA.getSigmaDiff( 1 )[0];
		vector < vector < double > > sigdifftopA = demA.getSigmaDiff( 0 )[0];
		vector < double > errA = demA.getGoodErrMeas( top, iObs );
		vector< TGraphErrors* > topaux = demA.getTopics()[0];
		ss << "AverA_" << observables[ iObs ][1];
		demA.drawTopics( 1, 1, 0, "all" ,ss.str());
		ss.str( string() );

		vector< double > sigrefA(2, 10e10);
		vector< double > sigtopA(2, 10e10);

		for ( int iTop = 0; iTop < sigdifftopA.size(); ++iTop ) {

			for ( int iRef = 0; iRef < sigdifftopA[ iTop ].size(); ++iRef ) {

				if ( sigdifftopA[ iTop ][ iRef ] < sigtopA[ iTop ] ) sigtopA[ iTop ] = sigdifftopA[ iTop ][ iRef ];
				if ( sigdiffrefA[ iTop ][ iRef ] < sigrefA[ iTop ] ) sigrefA[ iTop ] = sigdiffrefA[ iTop ][ iRef ];

			}
	

		}

		double sigmtopA = ( sigtopA[0] + sigtopA[1] ) / 2;
		double sigmrefA = ( sigrefA[0] + sigrefA[1] ) / 2;

		TGraph * gA = new TGraph( 2 ); 
		gA -> SetName( "gA" );

		if ( sigmrefA < 1.5 && sigmtopA < 1 && errA[0] < .05 && errA[1] < .05 ) {

			gA -> SetLineColorAlpha( kRed, 1 );


		} else if ( sigmrefA < 2 && sigmtopA < 1 && errA[0] < .05 && errA[1] < .05 ) {

			gA -> SetLineColorAlpha( kOrange, 1 );


		} else if ( sigmrefA < 3 && sigmtopA < 1 && errA[0] < .05 && errA[1] < .05 ) {

			gA -> SetLineColorAlpha( kYellow, 1 );


		} else if( sigmrefA < 3 && sigmtopA < 1 && errA[0] < .1 && errA[1] < .1 ){

			gA -> SetLineColorAlpha( kGreen, 1 );


		} else if( sigmrefA < 5 && sigmtopA < 1 ){

			gA -> SetLineColorAlpha( kBlue, 1 );


		} else {
		
			gA -> SetLineColorAlpha( kBlack, 1 );


		}

		gA -> SetPoint( 0, .5 + .5 * .5, .5 * sqrt( 3 ) / 2 );
		gA -> SetPoint( 1, 0 + .5 * .5, .5 * sqrt( 3 ) / 2 );
		
		gA -> SetLineWidth( 5 );

		vector< vector< TGraphErrors* > > aversBaux;
		vector< TGraphErrors* > aversB;
		vector< TGraphErrors* > auxB1;
		vector< TGraphErrors* > auxB2;
		auxB1.push_back( igrphs[ iObs ][1] );
		auxB1.push_back( igrphs[ iObs ][0] );
		auxB2.push_back( igrphs[ iObs ][1] );
		auxB2.push_back( igrphs[ iObs ][2] );
		aversB.push_back( getAver( auxB1 ) );
		aversB.push_back( getAver( auxB2 ) );
		aversBaux.push_back(aversB);

		demix demB(aversBaux,getGlims(),nam, rfilesafe,obs, pfilesafe );
		demB.Demix();
		vector< TGraphErrors* > topB = demB.getTopics()[0];
		vector < vector < double > > sigdiffrefB = demB.getSigmaDiff( 1 )[0];
		vector < vector < double > > sigdifftopB = demB.getSigmaDiff( 0 )[0];
		vector < double > errB = demB.getGoodErrMeas( top, iObs );
		ss << "AverB_" << observables[ iObs ][1];
		demB.drawTopics( 1, 1, 0, "all" ,ss.str());
		ss.str( string() );

		vector< double > sigrefB(2, 10e10);
		vector< double > sigtopB(2, 10e10);

		for ( int iTop = 0; iTop < sigdifftopB.size(); ++iTop ) {

			for ( int iRef = 0; iRef < sigdifftopB[ iTop ].size(); ++iRef ) {

				if ( sigdifftopB[ iTop ][ iRef ] < sigtopB[ iTop ] ) sigtopB[ iTop ] = sigdifftopB[ iTop ][ iRef ];
				if ( sigdiffrefB[ iTop ][ iRef ] < sigrefB[ iTop ] ) sigrefB[ iTop ] = sigdiffrefB[ iTop ][ iRef ];
			
							


			}
	

		}
		double sigmtopB = ( sigtopB[0] + sigtopB[1] ) / 2;
		double sigmrefB = ( sigrefB[0] + sigrefB[1] ) / 2;

		TGraph * gB = new TGraph( 2 ); 
		gB -> SetName( "gB" );

		if ( sigmrefB < 1.5 && sigmtopB < 1 && errB[0] < .05 && errB[1] < .05 ) {

			gB -> SetLineColorAlpha( kRed, 1 );


		} else if ( sigmrefB < 2 && sigmtopB < 1 && errB[0] < .05 && errB[1] < .05 ) {

			gB -> SetLineColorAlpha( kOrange, 1 );


		} else if ( sigmrefB < 3 && sigmtopB < 1 && errB[0] < .05 && errB[1] < .05 ) {

			gB -> SetLineColorAlpha( kYellow, 1 );


		} else if( sigmrefB < 3 && sigmtopB < 1 && errB[0] < .1 && errB[1] < .1 ){

			gB -> SetLineColorAlpha( kGreen, 1 );


		} else if( sigmrefB < 5 && sigmtopB < 1 ){

			gB -> SetLineColorAlpha( kBlue, 1 );


		} else {
		
			gB -> SetLineColorAlpha( kBlack, 1 );


		}

		gB -> SetPoint( 0, .5 + .5 * .5, .5 * sqrt( 3 ) / 2 );
		gB -> SetPoint( 1, .5 +  0 * .5,  0 * sqrt( 3 ) / 2 );
		
		c1 -> cd();
		gB -> SetLineWidth( 5 );

		vector< TGraphErrors* > aversC;
		vector< TGraphErrors* > auxC1;
		vector< TGraphErrors* > auxC2;
		auxC1.push_back( igrphs[ iObs ][2] );
		auxC1.push_back( igrphs[ iObs ][0] );
		auxC2.push_back( igrphs[ iObs ][2] );
		auxC2.push_back( igrphs[ iObs ][1] );
		aversC.push_back( getAver( auxC1 ) );
		aversC.push_back( getAver( auxC2 ) );

		demix demC(aversBaux,getGlims(),nam, rfilesafe,obs, pfilesafe );
		demC.Demix();
		vector< TGraphErrors* > topC = demC.getTopics()[0];
		vector < vector < double > > sigdiffrefC = demC.getSigmaDiff( 1 )[0];
		vector < vector < double > > sigdifftopC = demC.getSigmaDiff( 0 )[0];
		vector < double > errC = demC.getGoodErrMeas( top, iObs );
		ss << "AverC_" << observables[ iObs ][1];
		demC.drawTopics( 1, 1, 0, "all" ,ss.str());
		ss.str( string() );

		vector< double > sigrefC(2, 10e10);
		vector< double > sigtopC(2, 10e10);

		for ( int iTop = 0; iTop < sigdifftopC.size(); ++iTop ) {

			for ( int iRef = 0; iRef < sigdifftopC[ iTop ].size(); ++iRef ) {

				if ( sigdifftopC[ iTop ][ iRef ] < sigtopC[ iTop ] ) sigtopC[ iTop ] = sigdifftopC[ iTop ][ iRef ];
				if ( sigdiffrefC[ iTop ][ iRef ] < sigrefC[ iTop ] ) sigrefC[ iTop ] = sigdiffrefC[ iTop ][ iRef ];
			
							


			}
	

		}

		double sigmtopC = ( sigtopC[0] + sigtopC[1] ) / 2;
		double sigmrefC = ( sigrefC[0] + sigrefC[1] ) / 2;

		TGraph * gC = new TGraph(2); 
		gC -> SetName( "gC" );

		if ( sigmrefC < 1.5 && sigmtopC < 1 && errC[0] < .05 && errC[1] < .05 ) {

			gC -> SetLineColorAlpha( kRed, 1 );


		} else if ( sigmrefC < 2 && sigmtopC < 1 && errC[0] < .05 && errC[1] < .05 ) {

			gC -> SetLineColorAlpha( kOrange, 1 );


		} else if ( sigmrefC < 3 && sigmtopC < 1 && errC[0] < .05 && errC[1] < .05 ) {

			gC -> SetLineColorAlpha( kYellow, 1 );


		} else if( sigmrefC < 3 && sigmtopC < 1 && errC[0] < .1 && errC[1] < .1 ){

			gC -> SetLineColorAlpha( kGreen, 1 );


		} else if( sigmrefC < 5 && sigmtopC < 1 ){

			gC -> SetLineColorAlpha( kBlue, 1 );


		} else {
		
			gC -> SetLineColorAlpha( kBlack, 1 );


		}

		gC -> SetPoint( 0,  0 + .5 * .5, .5 * sqrt( 3 ) / 2 );
		gC -> SetPoint( 1, .5 +  0 * .5,  0 * sqrt( 3 ) / 2 );
		
		gC -> SetLineWidth( 5 );

		vector< TGraph* > g1(n);

		for ( int i = 0; i < n ; ++i ) {

			g1[i] = new TGraph(2);
			g1[i] -> SetName( to_string( i ).c_str() );

			vector< double > fracs_1;
			vector< double > fracs_2;
			vector< TGraphErrors* > convs;
			vector< vector< TGraphErrors* > > convsaux;
			convs.push_back( getConv( igrphs[ iObs ], fracs_1 ) );
			convs.push_back( getConv( igrphs[ iObs ], fracs_2 ) );
			convsaux.push_back( convs );
			vector< TGraphErrors* > top = SquareDemix( convs, iObs );
			vector < vector < double > > sigdiffref = getSigmaDiff( top, rgrphs[ iObs ], iObs, 1 );
			vector < vector < double > > sigdifftop = getSigmaDiff( top, rgrphs[ iObs ], iObs, 0 );
			vector < double > err = getGoodErrMeas( top, iObs );

			vector< double > sigref(2, 10e10);
			vector< double > sigtop(2, 10e10);

			for ( int iTop = 0; iTop < sigdifftop.size(); ++iTop ) {

				for ( int iRef = 0; iRef < sigdifftop[ iTop ].size(); ++iRef ) {

					if ( sigdifftop[ iTop ][ iRef ] < sigtop[ iTop ] ) sigtop[ iTop ] = sigdifftop[ iTop ][ iRef ];
					if ( sigdiffref[ iTop ][ iRef ] < sigref[ iTop ] ) sigref[ iTop ] = sigdiffref[ iTop ][ iRef ];
				
								


				}
	

			}

			double sigmtop = ( sigtop[0] + sigtop[1] ) / 2;
			double sigmref = ( sigref[0] + sigref[1] ) / 2;

			if ( sigmref < 1.5 && sigmtop < 1 && err[0] < .05 && err[1] < .05 ) {

				g1[i] -> SetLineColorAlpha( kRed, 0.5 );


			} else if( sigmref < 2 && sigmtop < 1 && err[0] < .05 && err[1] < .05 ){

				g1[i] -> SetLineColorAlpha( kOrange,0.05 );


			} else if ( sigmref < 3 && sigmtop < 1 && err[0] < .05 && err[1] < .05 ) {

				g1[i] -> SetLineColorAlpha( kYellow, 0.05 );


			} else if( sigmref < 3 && sigmtop < 1 && err[0] < .1 && err[1] < .1 ){

				g1[i] -> SetLineColorAlpha( kGreen, 0.05 );


			} else if( sigmref < 5 && sigmtop < 1  ){

				g1[i] -> SetLineColorAlpha( kBlue, 0.05 );


			} else {
			
				g1[i] -> SetLineColorAlpha( kBlack, 0.05 );


			}


			g1[i] -> SetPoint( 0, fracs_1[1] + .5 * fracs_1[0], fracs_1[0] * sqrt( 3 ) / 2 );
			g1[i] -> SetPoint( 1, fracs_2[1] + .5 * fracs_2[0], fracs_2[0] * sqrt( 3 ) / 2 );
			
			c1 -> cd();
			g1[i] -> Draw( "same L" );		


		}
	

		c1 -> cd();
		l   -> Draw( "same" );
		gA  -> Draw( "same L" );		
		gB  -> Draw( "same L" );		
		gC  -> Draw( "same L" );		
		gAB -> Draw( "same L" );
		gAC -> Draw( "same L" );
		gBC -> Draw( "same L" );
		ss << "testconv_" << observables[ iObs ][1] << ".pdf";
		c1 ->Print(ss.str().c_str());
		ss.str( string() );
		ss << "testconv_" << observables[ iObs ][1] << ".C";
		c1 ->Print(ss.str().c_str());

		delete gA ;
		delete gB ;
		delete gC ;
		delete gAB;
		delete gAC;
		delete gBC;
		for ( int iG = 0; iG < g1.size(); ++iG ) delete g1[iG]; 
		
	}
		

}
void demix::testConv1( int n , string A, string B, string C ){

	TernaryPlot* t1 = new TernaryPlot( 0 );
	t1 -> SetATitle(fnames[0][0]);
	t1 -> SetBTitle(fnames[1][0]);
	t1 -> SetCTitle(fnames[2][0]);
	TCanvas * c1 = new TCanvas( "c1" );
	t1 -> Draw("");

	TLegend* l = new TLegend( .1, .7, .59, .99, "", "NDC");
	
	bool cl1 = 1;
	bool cl2 = 1;
	bool cl3 = 1;
	bool cl4 = 1;
	bool cl5 = 1;
	bool cl6 = 1;
	
	TGraph gr1(0);	
	TGraph gr2(0);	
	TGraph gr3(0);	
	TGraph gr4(0);	
	TGraph gr5(0);	
	TGraph gr6(0);	

	gr1.SetName( "kRed" );
	gr2.SetName( "kOrange" );
	gr3.SetName( "kYellow" );
	gr4.SetName( "kGreen" );
	gr5.SetName( "kBlue" );
	gr6.SetName( "kBlack" );

	gr1.SetLineColor( kRed );
	gr2.SetLineColor( kOrange );
	gr3.SetLineColor( kYellow );
	gr4.SetLineColor( kGreen );
	gr5.SetLineColor( kBlue );
	gr6.SetLineColor( kBlack );

	l -> AddEntry( &gr1, "<#sigma>_{ref} < 1.5 && <#sigma>_{top} < 1 && E_{1} < .05 && E_{2} < .05", "l"  );
	l -> AddEntry( &gr2, "<#sigma>_{ref} < 2    && <#sigma>_{top} < 1 && E_{1} < .05 && E_{2} < .05", "l" );
	l -> AddEntry( &gr3, "<#sigma>_{ref} < 3    && <#sigma>_{top} < 1 && E_{1} < .05 && E_{2} < .05", "l" );
	l -> AddEntry( &gr4, "<#sigma>_{ref} < 3    && <#sigma>_{top} < 1 && E_{1} <  .1 && E_{2} <  .1", "l" );
	l -> AddEntry( &gr5, "<#sigma>_{ref} < 5    && <#sigma>_{top} < 1", "l" 			      );
	l -> AddEntry( &gr6, "all other options", "l" 							      );

	l -> SetFillStyle( 0 );
	l -> SetBorderSize( 0 );


	for ( int iObs = 0; iObs < igrphs.size();++iObs ) {

		cout << "[demix::TestConv]	testing observable " << observables[ iObs ][0] << "." << endl;


		vector< vector< TGraphErrors* > > ABaux;
		vector< TGraphErrors* > AB;
		AB.push_back( igrphs[ iObs ][0] );
		AB.push_back( igrphs[ iObs ][1] );

		vector< vector < string > > ABnam;
		vector< string > ABaux1;
		vector< string > ABaux2;
		ABaux1.push_back(fnames[0][0]);
		ABaux1.push_back("");
		ABaux2.push_back(fnames[1][0]);
		ABaux2.push_back("");
		ABnam.push_back(ABaux1);
		ABnam.push_back(ABaux2);
		ABaux.push_back(AB);

		vector< vector < string > > obs;
		obs.push_back( observables[ iObs ] );

		demix demAB(ABaux,getGlims(),ABnam, rfilesafe,obs, pfilesafe );
		demAB.Demix();
		vector< TGraphErrors* > topAB = demAB.getTopics()[0];
		vector < vector < double > > sigdiffrefAB = demAB.getSigmaDiff( 1 )[0];
		vector < vector < double > > sigdifftopAB = demAB.getSigmaDiff( 0 )[0];
		vector < double > errAB = demAB.getGoodErrMeas( topAB, iObs );
		
		stringstream ss;
		ss << "AB_" << observables[ iObs ][1];
		demAB.drawTopics( 1, 1, 0, "all", ss.str() );
		ss.str( string() );

		vector< double > sigrefAB(2, 10e10);
		vector< double > sigtopAB(2, 10e10);

		for ( int iTop = 0; iTop < sigdifftopAB.size(); ++iTop ) {

			for ( int iRef = 0; iRef < sigdifftopAB[ iTop ].size(); ++iRef ) {

				if ( sigdifftopAB[ iTop ][ iRef ] < sigtopAB[ iTop ] ) sigtopAB[ iTop ] = sigdifftopAB[ iTop ][ iRef ];
				if ( sigdiffrefAB[ iTop ][ iRef ] < sigrefAB[ iTop ] ) sigrefAB[ iTop ] = sigdiffrefAB[ iTop ][ iRef ];
			

			}
	

		}

		double sigmtopAB = 10e6; 
		double sigmrefAB = 10e6; 

		for ( int i = 0; i < 2 ; ++i ) {
			
			if ( sigtopAB[ i ] < sigmtopAB ) sigmtopAB = sigtopAB[i];
			if ( sigrefAB[ i ] < sigmrefAB ) sigmrefAB = sigrefAB[i];
			
		
		}

		TGraph * gAB = new TGraph( 2 ); 
		gAB -> SetName( "gAB" );

		if ( sigmrefAB < 1.5 && sigmtopAB < 1 && errAB[0] < .05 && errAB[1] < .05 ) {

			gAB -> SetLineColorAlpha( kRed, 1 );


		} else if ( sigmrefAB < 2 && sigmtopAB < 1 && errAB[0] < .05 && errAB[1] < .05 ) {

			gAB -> SetLineColorAlpha( kOrange, 1 );


		} else if ( sigmrefAB < 3 && sigmtopAB < 1 && errAB[0] < .05 && errAB[1] < .05 ) {

			gAB -> SetLineColorAlpha( kYellow, 1 );


		} else if( sigmrefAB < 3 && sigmtopAB < 1 && errAB[0] < .1 && errAB[1] < .1 ){

			gAB -> SetLineColorAlpha( kGreen, 1 );


		} else if( sigmrefAB < 5 && sigmtopAB < 1 ){

			gAB -> SetLineColorAlpha( kBlue, 1 );


		} else {
		
			gAB -> SetLineColorAlpha( kBlack, 1 );


		}

		gAB -> SetPoint( 0,  0 + 1 * .5 - .003, 1 * sqrt( 3 ) / 2 - .007 );
		gAB -> SetPoint( 1,  1 + 0 * .5 - .01, 0 * sqrt( 3 ) / 2 + .005 );
		
		gAB -> SetLineWidth( 3 );

		vector< vector< TGraphErrors* > > ACaux;
		vector< TGraphErrors* > AC;
		AC.push_back( igrphs[ iObs ][0] );
		AC.push_back( igrphs[ iObs ][2] );

		vector< vector < string > > ACnam;
		vector< string > ACaux1;
		vector< string > ACaux2;
		ACaux1.push_back(fnames[0][0]);
		ACaux1.push_back("");
		ACaux2.push_back(fnames[2][0]);
		ACaux2.push_back("");
		ACnam.push_back(ACaux1);
		ACnam.push_back(ACaux2);
		ACaux.push_back(AC);

		demix demAC(ACaux,getGlims(),ACnam, rfilesafe,obs, pfilesafe );
		demAC.Demix();
		vector< TGraphErrors* > topAC = demAC.getTopics()[0];
		vector < vector < double > > sigdiffrefAC = demAC.getSigmaDiff( 1 )[0];
		vector < vector < double > > sigdifftopAC = demAC.getSigmaDiff( 0 )[0];
		vector < double > errAC = demAC.getGoodErrMeas( topAC, iObs );
		ss << "AC_" << observables[ iObs ][1];
		demAC.drawTopics( 1, 1, 0, "all" ,ss.str());
		ss.str( string() );

		vector< double > sigrefAC(2, 10e10);
		vector< double > sigtopAC(2, 10e10);

		for ( int iTop = 0; iTop < sigdifftopAC.size(); ++iTop ) {

			for ( int iRef = 0; iRef < sigdifftopAC[ iTop ].size(); ++iRef ) {

				if ( sigdifftopAC[ iTop ][ iRef ] < sigtopAC[ iTop ] ) sigtopAC[ iTop ] = sigdifftopAC[ iTop ][ iRef ];
				if ( sigdiffrefAC[ iTop ][ iRef ] < sigrefAC[ iTop ] ) sigrefAC[ iTop ] = sigdiffrefAC[ iTop ][ iRef ];
			
							


			}
	

		}

		double sigmtopAC = 10e6; 
		double sigmrefAC = 10e6; 

		for ( int i = 0; i < 2 ; ++i ) {
			
			if ( sigtopAC[ i ] < sigmtopAC ) sigmtopAC = sigtopAC[i];
			if ( sigrefAC[ i ] < sigmrefAC ) sigmrefAC = sigrefAC[i];
			
		
		}

		TGraph * gAC = new TGraph( 2 ); 
		gAC -> SetName( "gAC" );

		if ( sigmrefAC < 1.5 && sigmtopAC < 1 && errAC[0] < .05 && errAC[1] < .05 ) {

			gAC -> SetLineColorAlpha( kRed, 1 );


		} else if ( sigmrefAC < 2 && sigmtopAC < 1 && errAC[0] < .05 && errAC[1] < .05 ) {
			gAC -> SetLineColorAlpha( kOrange, 1 );


		} else if ( sigmrefAC < 3 && sigmtopAC < 1 && errAC[0] < .05 && errAC[1] < .05 ) {

			gAC -> SetLineColorAlpha( kYellow, 1 );


		} else if( sigmrefAC < 3 && sigmtopAC < 1 && errAC[0] < .1 && errAC[1] < .1 ){

			gAC -> SetLineColorAlpha( kGreen, 1 );


		} else if( sigmrefAC < 5 && sigmtopAC < 1 ){

			gAC -> SetLineColorAlpha( kBlue, 1 );



		} else {
		
			gAC -> SetLineColorAlpha( kBlack, 1 );


		}

		gAC -> SetPoint( 0,  0 + 1 * .5 + .003, 1 * sqrt( 3 ) / 2 - .007 );
		gAC -> SetPoint( 1,  0  + 0 * .5 + .01 , 0 * sqrt( 3 ) / 2 + .005 );
		
		gAC -> SetLineWidth( 3 );

		vector< vector< TGraphErrors* > > BCaux;
		vector< TGraphErrors* > BC;
		BC.push_back( igrphs[ iObs ][1] );
		BC.push_back( igrphs[ iObs ][2] );

		vector< vector < string > > BCnam;
		vector< string > BCaux1;
		vector< string > BCaux2;
		BCaux1.push_back(fnames[1][0]);
		BCaux1.push_back("");
		BCaux2.push_back(fnames[2][0]);
		BCaux2.push_back("");
		BCnam.push_back(BCaux1);
		BCnam.push_back(BCaux2);
		BCaux.push_back(BC);

		demix demBC(BCaux,getGlims(),BCnam, rfilesafe,obs, pfilesafe );
		demBC.Demix();
		vector< TGraphErrors* > topBC = demBC.getTopics()[0];
		vector < vector < double > > sigdiffrefBC = demBC.getSigmaDiff( 1 )[0];
		vector < vector < double > > sigdifftopBC = demBC.getSigmaDiff( 0 )[0];
		vector < double > errBC = demBC.getGoodErrMeas( topBC, iObs );
		ss << "BC_" << observables[ iObs ][1];
		demBC.drawTopics( 1, 1, 0, "all" ,ss.str());
		ss.str( string() );

		vector< double > sigrefBC(2, 10e10);
		vector< double > sigtopBC(2, 10e10);

		for ( int iTop = 0; iTop < sigdifftopBC.size(); ++iTop ) {

			for ( int iRef = 0; iRef < sigdifftopBC[ iTop ].size(); ++iRef ) {

				if ( sigdifftopBC[ iTop ][ iRef ] < sigtopBC[ iTop ] ) sigtopBC[ iTop ] = sigdifftopBC[ iTop ][ iRef ];
				if ( sigdiffrefBC[ iTop ][ iRef ] < sigrefBC[ iTop ] ) sigrefBC[ iTop ] = sigdiffrefBC[ iTop ][ iRef ];


			}
	

		}

		double sigmtopBC = 10e6; 
		double sigmrefBC = 10e6; 

		for ( int i = 0; i < 2 ; ++i ) {
			
			if ( sigtopBC[ i ] < sigmtopBC ) sigmtopBC = sigtopBC[i];
			if ( sigrefBC[ i ] < sigmrefBC ) sigmrefBC = sigrefBC[i];
			
		
		}

		TGraph * gBC = new TGraph( 2 ); 
		gBC -> SetName( "gBC" );

		if ( sigmrefBC < 1.5 && sigmtopBC < 1 && errBC[0] < .05 && errBC[1] < .05 ) {

			gBC -> SetLineColorAlpha( kRed, 1 );


		} else if ( sigmrefBC < 2 && sigmtopBC < 1 && errBC[0] < .05 && errBC[1] < .05 ) {

			gBC -> SetLineColorAlpha( kOrange, 1 );


		} else if ( sigmrefBC < 3 && sigmtopBC < 1 && errBC[0] < .05 && errBC[1] < .05 ) {

			gBC -> SetLineColorAlpha( kYellow, 1 );


		} else if( sigmrefBC < 3 && sigmtopBC < 1 && errBC[0] < .1 && errBC[1] < .1 ){

			gBC -> SetLineColorAlpha( kGreen, 1 );


		} else if( sigmrefBC < 5 && sigmtopBC < 1 ){

			gBC -> SetLineColorAlpha( kBlue, 1 );



		} else {
		
			gBC -> SetLineColorAlpha( kBlack, 1 );


		}

		gBC -> SetPoint( 0,  0 + 0 * .5 + .01, 0 * sqrt( 3 ) / 2 + .007 );
		gBC -> SetPoint( 1,  1 + 0 * .5 - .01, 0 * sqrt( 3 ) / 2 + .007 );
		
		gBC -> SetLineWidth( 3 );

		vector< vector< TGraphErrors* > > aversAaux;
		vector< TGraphErrors* > aversA;
		vector< TGraphErrors* > auxA1;
		vector< TGraphErrors* > auxA2;
		auxA1.push_back( igrphs[ iObs ][0] );
		auxA1.push_back( igrphs[ iObs ][1] );
		auxA2.push_back( igrphs[ iObs ][0] );
		auxA2.push_back( igrphs[ iObs ][2] );
		aversA.push_back( getAver( auxA1 ) );
		aversA.push_back( getAver( auxA2 ) );

		vector< vector < string > > nam;
		vector< string > aux1;
		vector< string > aux2;
		aux1.push_back("aver1");
		aux1.push_back("");
		aux2.push_back("aver2");
		aux2.push_back("");
		nam.push_back(aux1);
		nam.push_back(aux2);
		aversAaux.push_back(aversA);

		demix demA(aversAaux,getGlims(),nam, rfilesafe,obs, pfilesafe );
		demA.Demix();
		vector< TGraphErrors* > top = demA.getTopics()[0];
		vector < vector < double > > sigdiffrefA = demA.getSigmaDiff( 1 )[0];
		vector < vector < double > > sigdifftopA = demA.getSigmaDiff( 0 )[0];
		vector < double > errA = demA.getGoodErrMeas( top, iObs );
		vector< TGraphErrors* > topaux = demA.getTopics()[0];
		ss << "AverA_" << observables[ iObs ][1];
		demA.drawTopics( 1, 1, 0, "all" ,ss.str());
		ss.str( string() );

		vector< double > sigrefA(2, 10e10);
		vector< double > sigtopA(2, 10e10);

		for ( int iTop = 0; iTop < sigdifftopA.size(); ++iTop ) {

			for ( int iRef = 0; iRef < sigdifftopA[ iTop ].size(); ++iRef ) {

				if ( sigdifftopA[ iTop ][ iRef ] < sigtopA[ iTop ] ) sigtopA[ iTop ] = sigdifftopA[ iTop ][ iRef ];
				if ( sigdiffrefA[ iTop ][ iRef ] < sigrefA[ iTop ] ) sigrefA[ iTop ] = sigdiffrefA[ iTop ][ iRef ];
			

			}
	

		}

		double sigmtopA = 10e6; 
		double sigmrefA = 10e6; 

		for ( int i = 0; i < 2 ; ++i ) {
			
			if ( sigtopA[ i ] < sigmtopA ) sigmtopA = sigtopA[i];
			if ( sigrefA[ i ] < sigmrefA ) sigmrefA = sigrefA[i];
			
		
		}

		TGraph * gA = new TGraph( 2 ); 
		gA -> SetName( "gA" );

		if ( sigmrefA < 1.5 && sigmtopA < 1 && errA[0] < .05 && errA[1] < .05 ) {

			gA -> SetLineColorAlpha( kRed, 1 );


		} else if ( sigmrefA < 2 && sigmtopA < 1 && errA[0] < .05 && errA[1] < .05 ) {

			gA -> SetLineColorAlpha( kOrange, 1 );


		} else if ( sigmrefA < 3 && sigmtopA < 1 && errA[0] < .05 && errA[1] < .05 ) {

			gA -> SetLineColorAlpha( kYellow, 1 );


		} else if( sigmrefA < 3 && sigmtopA < 1 && errA[0] < .1 && errA[1] < .1 ){

			gA -> SetLineColorAlpha( kGreen, 1 );


		} else if( sigmrefA < 5 && sigmtopA < 1 ){

			gA -> SetLineColorAlpha( kBlue, 1 );


		} else {
		
			gA -> SetLineColorAlpha( kBlack, 1 );


		}

		gA -> SetPoint( 0, .5 + .5 * .5, .5 * sqrt( 3 ) / 2 );
		gA -> SetPoint( 1, 0 + .5 * .5, .5 * sqrt( 3 ) / 2 );
		
		gA -> SetLineWidth( 5 );

		vector< vector< TGraphErrors* > > aversBaux;
		vector< TGraphErrors* > aversB;
		vector< TGraphErrors* > auxB1;
		vector< TGraphErrors* > auxB2;
		auxB1.push_back( igrphs[ iObs ][1] );
		auxB1.push_back( igrphs[ iObs ][0] );
		auxB2.push_back( igrphs[ iObs ][1] );
		auxB2.push_back( igrphs[ iObs ][2] );
		aversB.push_back( getAver( auxB1 ) );
		aversB.push_back( getAver( auxB2 ) );
		aversBaux.push_back(aversB);

		demix demB(aversBaux,getGlims(),nam, rfilesafe,obs, pfilesafe );
		demB.Demix();
		vector< TGraphErrors* > topB = demB.getTopics()[0];
		vector < vector < double > > sigdiffrefB = demB.getSigmaDiff( 1 )[0];
		vector < vector < double > > sigdifftopB = demB.getSigmaDiff( 0 )[0];
		vector < double > errB = demB.getGoodErrMeas( top, iObs );
		ss << "AverB_" << observables[ iObs ][1];
		demB.drawTopics( 1, 1, 0, "all" ,ss.str());
		ss.str( string() );

		vector< double > sigrefB(2, 10e10);
		vector< double > sigtopB(2, 10e10);

		for ( int iTop = 0; iTop < sigdifftopB.size(); ++iTop ) {

			for ( int iRef = 0; iRef < sigdifftopB[ iTop ].size(); ++iRef ) {

				if ( sigdifftopB[ iTop ][ iRef ] < sigtopB[ iTop ] ) sigtopB[ iTop ] = sigdifftopB[ iTop ][ iRef ];
				if ( sigdiffrefB[ iTop ][ iRef ] < sigrefB[ iTop ] ) sigrefB[ iTop ] = sigdiffrefB[ iTop ][ iRef ];
			
							


			}
	

		}

		double sigmtopB = 10e6; 
		double sigmrefB = 10e6; 

		for ( int i = 0; i < 2 ; ++i ) {
			
			if ( sigtopB[ i ] < sigmtopB ) sigmtopB = sigtopB[i];
			if ( sigrefB[ i ] < sigmrefB ) sigmrefB = sigrefB[i];
			
		
		}

		TGraph * gB = new TGraph( 2 ); 
		gB -> SetName( "gB" );

		if ( sigmrefB < 1.5 && sigmtopB < 1 && errB[0] < .05 && errB[1] < .05 ) {

			gB -> SetLineColorAlpha( kRed, 1 );


		} else if ( sigmrefB < 2 && sigmtopB < 1 && errB[0] < .05 && errB[1] < .05 ) {

			gB -> SetLineColorAlpha( kOrange, 1 );


		} else if ( sigmrefB < 3 && sigmtopB < 1 && errB[0] < .05 && errB[1] < .05 ) {

			gB -> SetLineColorAlpha( kYellow, 1 );


		} else if( sigmrefB < 3 && sigmtopB < 1 && errB[0] < .1 && errB[1] < .1 ){

			gB -> SetLineColorAlpha( kGreen, 1 );


		} else if( sigmrefB < 5 && sigmtopB < 1 ){

			gB -> SetLineColorAlpha( kBlue, 1 );


		} else {
		
			gB -> SetLineColorAlpha( kBlack, 1 );


		}

		gB -> SetPoint( 0, .5 + .5 * .5, .5 * sqrt( 3 ) / 2 );
		gB -> SetPoint( 1, .5 +  0 * .5,  0 * sqrt( 3 ) / 2 );
		
		c1 -> cd();
		gB -> SetLineWidth( 5 );

		vector< TGraphErrors* > aversC;
		vector< TGraphErrors* > auxC1;
		vector< TGraphErrors* > auxC2;
		auxC1.push_back( igrphs[ iObs ][2] );
		auxC1.push_back( igrphs[ iObs ][0] );
		auxC2.push_back( igrphs[ iObs ][2] );
		auxC2.push_back( igrphs[ iObs ][1] );
		aversC.push_back( getAver( auxC1 ) );
		aversC.push_back( getAver( auxC2 ) );

		demix demC(aversBaux,getGlims(),nam, rfilesafe,obs, pfilesafe );
		demC.Demix();
		vector< TGraphErrors* > topC = demC.getTopics()[0];
		vector < vector < double > > sigdiffrefC = demC.getSigmaDiff( 1 )[0];
		vector < vector < double > > sigdifftopC = demC.getSigmaDiff( 0 )[0];
		vector < double > errC = demC.getGoodErrMeas( top, iObs );
		ss << "AverC_" << observables[ iObs ][1];
		demC.drawTopics( 1, 1, 0, "all" ,ss.str());
		ss.str( string() );

		vector< double > sigrefC(2, 10e10);
		vector< double > sigtopC(2, 10e10);

		for ( int iTop = 0; iTop < sigdifftopC.size(); ++iTop ) {

			for ( int iRef = 0; iRef < sigdifftopC[ iTop ].size(); ++iRef ) {

				if ( sigdifftopC[ iTop ][ iRef ] < sigtopC[ iTop ] ) sigtopC[ iTop ] = sigdifftopC[ iTop ][ iRef ];
				if ( sigdiffrefC[ iTop ][ iRef ] < sigrefC[ iTop ] ) sigrefC[ iTop ] = sigdiffrefC[ iTop ][ iRef ];
			
							


			}
	

		}

		double sigmtopC = 10e6; 
		double sigmrefC = 10e6; 

		for ( int i = 0; i < 2 ; ++i ) {
			
			if ( sigtopC[ i ] < sigmtopC ) sigmtopC = sigtopC[i];
			if ( sigrefC[ i ] < sigmrefC ) sigmrefC = sigrefC[i];
			
		
		}

		TGraph * gC = new TGraph(2); 
		gC -> SetName( "gC" );

		if ( sigmrefC < 1.5 && sigmtopC < 1 && errC[0] < .05 && errC[1] < .05 ) {

			gC -> SetLineColorAlpha( kRed, 1 );


		} else if ( sigmrefC < 2 && sigmtopC < 1 && errC[0] < .05 && errC[1] < .05 ) {

			gC -> SetLineColorAlpha( kOrange, 1 );


		} else if ( sigmrefC < 3 && sigmtopC < 1 && errC[0] < .05 && errC[1] < .05 ) {

			gC -> SetLineColorAlpha( kYellow, 1 );


		} else if( sigmrefC < 3 && sigmtopC < 1 && errC[0] < .1 && errC[1] < .1 ){

			gC -> SetLineColorAlpha( kGreen, 1 );


		} else if( sigmrefC < 5 && sigmtopC < 1 ){

			gC -> SetLineColorAlpha( kBlue, 1 );



		} else {
		
			gC -> SetLineColorAlpha( kBlack, 1 );


		}


		gC -> SetPoint( 0,  0 + .5 * .5, .5 * sqrt( 3 ) / 2 );
		gC -> SetPoint( 1, .5 +  0 * .5,  0 * sqrt( 3 ) / 2 );
		
		gC -> SetLineWidth( 5 );

		vector< TGraph* > g1(n);

		for ( int i = 0; i < n ; ++i ) {

			g1[i] = new TGraph(2);
			g1[i] -> SetName( to_string( i ).c_str() );

			vector< double > fracs_1;
			vector< double > fracs_2;
			vector< TGraphErrors* > convs;
			vector< vector< TGraphErrors* > > convsaux;
			convs.push_back( getConv( igrphs[ iObs ], fracs_1 ) );
			convs.push_back( getConv( igrphs[ iObs ], fracs_2 ) );
			convsaux.push_back( convs );
			vector< TGraphErrors* > top = SquareDemix( convs, iObs );
			vector < vector < double > > sigdiffref = getSigmaDiff( top, rgrphs[ iObs ], iObs, 1 );
			vector < vector < double > > sigdifftop = getSigmaDiff( top, rgrphs[ iObs ], iObs, 0 );
			vector < double > err = getGoodErrMeas( top, iObs );

			vector< double > sigref(2, 10e10);
			vector< double > sigtop(2, 10e10);

			for ( int iTop = 0; iTop < sigdifftop.size(); ++iTop ) {

				for ( int iRef = 0; iRef < sigdifftop[ iTop ].size(); ++iRef ) {

					if ( sigdifftop[ iTop ][ iRef ] < sigtop[ iTop ] ) sigtop[ iTop ] = sigdifftop[ iTop ][ iRef ];
					if ( sigdiffref[ iTop ][ iRef ] < sigref[ iTop ] ) sigref[ iTop ] = sigdiffref[ iTop ][ iRef ];
				
								


				}
	

			}

			double sigmtop = 10e6; 
			double sigmref = 10e6; 

			for ( int j = 0; j < 2 ; ++j ) {
				
				if ( sigtop[ j ] < sigmtop ) sigmtop = sigtop[j];
				if ( sigref[ j ] < sigmref ) sigmref = sigref[j];
				
			
			}

			if ( sigmref < 1.5 && sigmtop < 1 && err[0] < .05 && err[1] < .05 ) {

				g1[i] -> SetLineColorAlpha( kRed, 0.5 );


			} else if( sigmref < 2 && sigmtop < 1 && err[0] < .05 && err[1] < .05 ){

				g1[i] -> SetLineColorAlpha( kOrange,0.05 );


			} else if ( sigmref < 3 && sigmtop < 1 && err[0] < .05 && err[1] < .05 ) {

				g1[i] -> SetLineColorAlpha( kYellow, 0.05 );


			} else if( sigmref < 3 && sigmtop < 1 && err[0] < .1 && err[1] < .1 ){

				g1[i] -> SetLineColorAlpha( kGreen, 0.05 );


			} else if( sigmref < 5 && sigmtop < 1  ){

				g1[i] -> SetLineColorAlpha( kBlue, 0.05 );


			} else {
			
				g1[i] -> SetLineColorAlpha( kBlack, 0.05 );


			}


			g1[i] -> SetPoint( 0, fracs_1[1] + .5 * fracs_1[0], fracs_1[0] * sqrt( 3 ) / 2 );
			g1[i] -> SetPoint( 1, fracs_2[1] + .5 * fracs_2[0], fracs_2[0] * sqrt( 3 ) / 2 );
			
			c1 -> cd();
			g1[i] -> Draw( "same L" );		


		}
	

		c1 -> cd();
		l   -> Draw( "same" );
		gA  -> Draw( "same L" );		
		gB  -> Draw( "same L" );		
		gC  -> Draw( "same L" );		
		gAB -> Draw( "same L" );
		gAC -> Draw( "same L" );
		gBC -> Draw( "same L" );
		ss << "testconv_" << observables[ iObs ][1] << ".pdf";
		c1 ->Print(ss.str().c_str());
		ss.str( string() );
		ss << "testconv_" << observables[ iObs ][1] << ".C";
		c1 ->Print(ss.str().c_str());

		delete gA ;
		delete gB ;
		delete gC ;
		delete gAB;
		delete gAC;
		delete gBC;
		for ( int iG = 0; iG < g1.size(); ++iG ) delete g1[iG]; 
		
	}
		

}

void demix::testConv2( int n , int tol, string A, string B ){
	
	TH2D * h = new TH2D( "", "", 1000, 0, 1, 1000, 0, 1 );
	TH2D * g = new TH2D( "", "", 10000, 0, 1, 8000, -1, 7 );
	g -> SetMarkerStyle( 1 );
	stringstream ss;	
	ss << fnames[0][0] << " as the \"special\" distributions";
	gStyle -> SetOptTitle(0);
	g -> SetTitle(ss.str().c_str() );
	ss.str( string() );
	ss << "Convex element fraction of " << fnames[1][0];
	g -> GetXaxis() -> SetTitle(ss.str().c_str() );
	ss.str( string() );

	g -> GetYaxis() -> SetBinLabel( 1000, "FaceTest Failed" );
	g -> GetYaxis() -> SetBinLabel( 7000, "<#sigma>_{ref} < 1.5 && <#sigma>_{top} < 1 && E_{1} < .05 && E_{2} < .05"   );
	g -> GetYaxis() -> SetBinLabel( 6000, "<#sigma>_{ref} < 2    && <#sigma>_{top} < 1 && E_{1} < .05 && E_{2} < .05"  );
	g -> GetYaxis() -> SetBinLabel( 5000, "<#sigma>_{ref} < 3    && <#sigma>_{top} < 1 && E_{1} < .05 && E_{2} < .05"  );
	g -> GetYaxis() -> SetBinLabel( 4000, "<#sigma>_{ref} < 3    && <#sigma>_{top} < 1 && E_{1} <  .1 && E_{2} <  .1"  );
	g -> GetYaxis() -> SetBinLabel( 3000, "<#sigma>_{ref} < 5    && <#sigma>_{top} < 1" 			       );
	g -> GetYaxis() -> SetBinLabel( 2000, "all other options" 							       );

	for ( int i = 0; i < n ; ++i ) {
		
		vector< double > fracs;
		int r = SquareDemix( igrphs[0], 0, fracs, tol );
	
		g -> Fill( fracs[0], r, 1 );
		h -> Fill( fracs[0], fracs[1], r );


	}

	TCanvas * c = new TCanvas( "c", "c", 700, 500 );

	g -> Draw( "" );
	gPad -> Print( "testconv_3tg.pdf" );
	gPad -> Print( "testconv_3tg.C" );
	gPad->WaitPrimitive();


}




void demix::testconv4( int n, int iObs, int tol ) {

	TernaryPlot* t1 = new TernaryPlot( n );
	TernaryPlot* t2 = new TernaryPlot( n );
	TernaryPlot* t3 = new TernaryPlot( n );
	TernaryPlot* t4 = new TernaryPlot( n );
	TernaryPlot* t5 = new TernaryPlot( n );
	TernaryPlot* t6 = new TernaryPlot( n );

	t1 -> SetATitle(fnames[1][0]);
	t1 -> SetBTitle(fnames[2][0]);
	t1 -> SetCTitle(fnames[3][0]);

	t2 -> SetATitle(fnames[1][0]);
	t2 -> SetBTitle(fnames[2][0]);
	t2 -> SetCTitle(fnames[3][0]);

	t3 -> SetATitle(fnames[1][0]);
	t3 -> SetBTitle(fnames[2][0]);
	t3 -> SetCTitle(fnames[3][0]);

	t4 -> SetATitle(fnames[1][0]);
	t4 -> SetBTitle(fnames[2][0]);
	t4 -> SetCTitle(fnames[3][0]);

	t5 -> SetATitle(fnames[1][0]);
	t5 -> SetBTitle(fnames[2][0]);
	t5 -> SetCTitle(fnames[3][0]);

	t6 -> SetATitle(fnames[1][0]);
	t6 -> SetBTitle(fnames[2][0]);
	t6 -> SetCTitle(fnames[3][0]);

	t6 -> SetMarkerColor( 632 );
	t5 -> SetMarkerColor( 800 );
	t4 -> SetMarkerColor( 400 );
	t3 -> SetMarkerColor( 416 );
	t2 -> SetMarkerColor( 600 );
	t1 -> SetMarkerColor( 1 );

	TCanvas * c1 = new TCanvas( "c1" );

	TLegend* l = new TLegend( .1, .7, .59, .99, "", "NDC");
	
	bool cl1 = 1;
	bool cl2 = 1;
	bool cl3 = 1;
	bool cl4 = 1;
	bool cl5 = 1;
	bool cl6 = 1;
	
	TGraph gr1(0);	
	TGraph gr2(0);	
	TGraph gr3(0);	
	TGraph gr4(0);	
	TGraph gr5(0);	
	TGraph gr6(0);	

	gr1.SetName( "kRed" );
	gr2.SetName( "kOrange" );
	gr3.SetName( "kYellow" );
	gr4.SetName( "kGreen" );
	gr5.SetName( "kBlue" );
	gr6.SetName( "kBlack" );

	gr1.SetMarkerColor( kRed );
	gr2.SetMarkerColor( kOrange );
	gr3.SetMarkerColor( kYellow );
	gr4.SetMarkerColor( kGreen );
	gr5.SetMarkerColor( kBlue );
	gr6.SetMarkerColor( kBlack );

	l -> AddEntry( &gr1, "<#sigma>_{ref} < 1.5 && <#sigma>_{top} < 1 && E_{1} < .05 && E_{2} < .05",  "p" );
	l -> AddEntry( &gr2, "<#sigma>_{ref} < 2    && <#sigma>_{top} < 1 && E_{1} < .05 && E_{2} < .05", "p" );
	l -> AddEntry( &gr3, "<#sigma>_{ref} < 3    && <#sigma>_{top} < 1 && E_{1} < .05 && E_{2} < .05", "p" );
	l -> AddEntry( &gr4, "<#sigma>_{ref} < 3    && <#sigma>_{top} < 1 && E_{1} <  .1 && E_{2} <  .1", "p" );
	l -> AddEntry( &gr5, "<#sigma>_{ref} < 5    && <#sigma>_{top} < 1", 				  "p" );
	l -> AddEntry( &gr6, "all other options", 							  "p" );

	l -> SetFillStyle( 0 );
	l -> SetBorderSize( 0 );

	l -> Draw( "same" ); 

	for ( int i = 0; i < n ; ++i ) {

		cout << "Run: " << i << endl;

		vector< double > fracs;
		int r = SquareDemix( igrphs[0], 0, fracs, tol );

		if ( r == 6 ) {

			t6 -> SetPoint( fracs[0], fracs[1], "AB" );


		} else if(  r == 5 ){

			t5 -> SetPoint( fracs[0], fracs[1], "AB" );


		} else if ( r == 4 ){

			t4 -> SetPoint( fracs[0], fracs[1], "AB" );


		} else if( r == 3 ){

			t3 -> SetPoint( fracs[0], fracs[1], "AB" );


		} else if( r == 2  ){

			t2 -> SetPoint( fracs[0], fracs[1], "AB" );


		} else {
		
			t1 -> SetPoint( fracs[0], fracs[1], "AB" );


		}


		
		c1 -> cd();


	}
		
	t1 -> Draw( "" );
	t2 -> Draw( "same" );
	t3 -> Draw( "same" );
	t4 -> Draw( "same" );
	t5 -> Draw( "same" );
	t6 -> Draw( "same" );
	gPad -> Print( "testconv4.pdf" );
	
	
}




void demix::testconv3( int n, int iObs ) {

	TFile * f = new TFile( "../macros/dummy.root","RECREATE" );
	TTree * t = new TTree( "t", "" );
	
	double f11 = 0;
	double f12 = 0;
	double f13 = 0;
	double f21 = 0;
	double f22 = 0;
	double f23 = 0;
	int cla = 5;
	double E1  = 0;
	double E2  = 0;
	double st  = 0;
	double sr  = 0;

	t -> Branch( "f11", &f11 );
	t -> Branch( "f12", &f12 );
	t -> Branch( "f13", &f13 );
	t -> Branch( "f21", &f21 );
	t -> Branch( "f22", &f22 );
	t -> Branch( "f23", &f23 );
	t -> Branch( "cla", &cla );
	t -> Branch( "E1" , &E1  );
	t -> Branch( "E2" , &E2  );
	t -> Branch( "st" , &st  );
	t -> Branch( "sr" , &sr  );

	for ( int i = 0; i < n; ++i ) {
	
		vector< double > fracs_1;
		vector< double > fracs_2;
		vector< TGraphErrors* > convs;
		vector< vector< TGraphErrors* > > convsaux;
		convs.push_back( getConv( igrphs[ iObs ], fracs_1 ) );
		convs.push_back( getConv( igrphs[ iObs ], fracs_2 ) );
		convsaux.push_back( convs );
		vector< TGraphErrors* > top = SquareDemix( convs, iObs );
		vector < vector < double > > sigdiffref = getSigmaDiff( top, rgrphs[ iObs ], iObs, 1 );
		vector < vector < double > > sigdifftop = getSigmaDiff( top, rgrphs[ iObs ], iObs, 0 );
		vector < double > err = getGoodErrMeas( top, iObs );

		f11 = fracs_1[0];
		f12 = fracs_1[1];
		f13 = fracs_1[2];
		f21 = fracs_2[0];
		f22 = fracs_2[1];
		f23 = fracs_2[2];
		E1 = err[0];
		E2 = err[1];

		vector< double > sigref(2, 10e10);
		vector< double > sigtop(2, 10e10);

		for ( int iTop = 0; iTop < sigdifftop.size(); ++iTop ) {

			for ( int iRef = 0; iRef < sigdifftop[ iTop ].size(); ++iRef ) {

				if ( sigdifftop[ iTop ][ iRef ] < sigtop[ iTop ] ) sigtop[ iTop ] = sigdifftop[ iTop ][ iRef ];
				if ( sigdiffref[ iTop ][ iRef ] < sigref[ iTop ] ) sigref[ iTop ] = sigdiffref[ iTop ][ iRef ];
			
							


			}


		}

		double sigmtop = ( sigtop[0] + sigtop[1] ) / 2;
		double sigmref = ( sigref[0] + sigref[1] ) / 2;

		st = sigtop[0];
		sr = sigref[0];

		if ( E1  < 0.05 && E2 < 0.05 && st < 1 && sr < 1.5 ) {
		
			cla = 0;
		t -> Fill();

		
		} else if ( E1  < 0.05 && E2 < 0.05 && st < 1 && sr < 2 ) {
		
			cla = 1;
		t -> Fill();
		
		
		} else if ( E1  < 0.05 && E2 < 0.05 && st < 1 && sr < 3 ) {
		
			cla = 2;
		

		} else if ( E1  < 0.1 && E2 < 0.1 && st < 1 && sr < 3 ) {
		
			cla = 3;

		
		} else if ( st < 1 && sr < 5 ) {
		
			cla = 4;

		
		} else {
		
			cla = 5;
		
		
		}



	}

	t -> Write();

	//t -> Draw("f11:f12:f13:f21:f22:f23", "", "para" );
	//gPad -> Print("testconv3.pdf");
	//TParallelCoord* para = (TParallelCoord*)gPad->GetListOfPrimitives()->FindObject("ParaCoord");
	////para -> SetLineStyle( 2 );
	//Double_t min = 0;
	//Double_t max = 1;
 	////para -> SetGlobalMin( min ); 
 	////para -> SetGlobalMax( max ); 
	//para -> SetAxisHistogramHeight( 0 );
	//para -> SetAxisHistogramLineWidth( 0 );
	//gPad -> Modified();
//	TObjLink *lnk = gPad -> GetListOfPrimitives()->FirstLink();
//	while (lnk) {
//		cout << lnk -> GetObject() -> GetName() << endl;
//		lnk = lnk->Next();
//	}

}


vector< string > demix::parseString( string line, string deli) {

	vector< string > out;

	string aux = line;

	size_t pos = 0;

	while( ( pos = aux.find( deli ) ) != string::npos ) {

		out.push_back( aux.substr( 0, pos ) );
		
		aux.erase( 0, pos + deli.length() );

	}

	out.push_back( aux );

	return out;

}

