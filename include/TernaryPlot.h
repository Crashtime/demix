
#include "TROOT.h"
#include "TMath.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TGaxis.h"
#include "TPolyMarker.h"
#include "TStyle.h"
#include "TF1.h"
#include "TH2D.h"
#include "TAxis.h"
#include <iostream>

using namespace std;

class TernaryPlot {

protected:
   Int_t              fNpoints;   // Number of points
   Int_t              fMaxSize;   // Current dimension of arrays fX and fY

   string name;

   Double_t          *fX;         // [fNpoints] array of X points
   Double_t          *fY;         // [fNpoints] array of Y points
   Double_t yC;

   TGaxis *a1;
   TGaxis *a2;
   TGaxis *a3;

   TPolyMarker* ternaryPlot;


public:

   ~TernaryPlot();
   TernaryPlot(Int_t n);
   void SetPoint(Double_t u, Double_t v, Option_t *option);
   void Draw( string );
   void SetATitle( string );
   void SetBTitle( string );
   void SetCTitle( string );
   void SetMarkerColor( int );
};


