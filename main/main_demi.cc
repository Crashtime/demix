#include "demix.h"
#include "TPad.h"
#include "TRint.h"
#include "TCanvas.h"
#include "TParallelCoord.h"

using namespace std;


int main( int argc, char* argv[] ) {		// Add verbose mode
	

	demix dem( argv[1] ,argv[2] ,argv[3], argv[4] );

	dem.Demix();
	
	dem.drawTopics( 1, 1 , 0, "all", "" );

      	dem.testConv1( 30000 );
	vector< vector< vector< double > > > fracs = dem.getAfrcs();
	vector< vector< vector< double > > > chi2t = dem.getChi2T();
	vector< vector< double > > 	     errms = dem.getGoodErrMeas();
      	vector< vector< vector< double > > > sigma = dem.getSigmaDiff(1);

      	//dem.testConv( 30000 );
 
	//TRint * app = new TRint( "Rint", &argc, argv );
	//int i;
	//cin >> i;
	//dem.testconv4( 1000, 0, 100 );

	for ( int iObs = 0; iObs < fracs.size(); ++iObs ) {

		stringstream ss_c;
		stringstream ss_e;
		stringstream ss_s;
	
		for ( int iTop = 0; iTop < chi2t[ iObs ].size(); ++iTop ) {

			for ( int iFil = 0; iFil < fracs[ iObs ].size(); ++iFil ) {

				ss_c << fracs[ iObs ][ iFil ][ iTop ] << " +- " << fracs[ iObs ][ iFil ][2] << "	";  


			} 		
	
			for ( int iRef = 0; iRef < chi2t[ iObs ][ iTop ].size(); ++iRef ) {
	
				ss_c << chi2t[ iObs ][ iTop ][ iRef ] << "	"; 
				ss_s << sigma[ iObs ][ iTop ][ iRef ] << "	";

			} 

			ss_c << "\n";	
			ss_e << errms[ iObs ][ iTop ] << "	";  


		}

		cout << "fractions anc chi2's \n" << ss_c.str();
		cout << "goodness of error measure \n" << ss_e.str() << endl;
		cout << "average sigma \n" << ss_s.str() << endl;


	}

	return 0;


}
