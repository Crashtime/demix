#ifndef __DEMIX_H_INCLUDED__ 
#define __DEMIX_H_INCLUDED__

// Standar headers
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <string> 
#include <sstream>
#include <time.h>
#include <math.h>
 
// ROOT headers
#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TProfile.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TKey.h"
#include "TParallelCoord.h"

// My headers
//#include "myutility.h"
#include "TernaryPlot.h"

// Class of functions implementing demix
class demix {

	public:
	// Source path for the ROOT files 
	std::string fpath;
	std::string rpath;

	std::string ifilesafe;	
	std::string rfilesafe;
	std::string ofilesafe;
	std::string pfilesafe;

	// Name of input root files and observables to analyse
	std::vector< std::vector< std::string > > fnames;				// Input files' names
	std::vector< std::vector< std::string > > rnames;				// Reference files' names
	std::vector< std::vector< std::string > > observables;				// Observable names

	// Parameters ( read from parameter.txt file )
	double      percent  	= 100.;					// Percent of histogram to consider
	int         numbtop 	= 2 ;					// Number of topics
	bool        bovf	= 0 ;					// Boolean control over keeping under and over flow bins
	int 	    palette  	= 57;					// Color palete used for drawing topics
	bool 	    norm  	= 1;					// Normalize distributions before? (in case you have samples with different size
	int 	    meto  	= 1;					// Method of computing kappas
	int 	    hard  	= 1;					
	int 	    inlims  	= 1;					
	int 	    rb  	= 1;					
	double 	    mincount	= 0;
	double 	    maxerror 	= 1;

	// Vectors for ROOT objects
	std::vector< TFile*	 		  > ifiles;		// Vector of input files
	std::vector< TFile*	 		  > rfiles;		// Vector of reference files
	std::vector< std::vector< TH1D* >	  > ihists;		// One vector of histograms per observable for all input files in another vector 
	std::vector< std::vector< TH1D* >	  > rhists;		// One vector of histograms per observable for all reference files in another vector 
	std::vector< std::vector< TGraphErrors* > > igrphs;		// One vector of graphs per observable for all input files in another vector 
	std::vector< std::vector< TGraphErrors* > > rgrphs;		// One vector of graphs per observable for all reference files in another vector 
	std::vector< std::vector< TGraphErrors* > > topics;		// One vector per observable with reconstructed topics
	std::vector< std::vector< std::vector< TGraphErrors* > > > kgraphs;		// One vector of histograms per observable containing both reducibility factors evolution with bin 
	std::vector< TMultiGraph * > kmg;
	std::vector< TMultiGraph * > mgr;

	// Limit vectors
	std::vector< std::vector< std::vector< int > > > lims;		// One vector of limits (also vector) per observable for all files in another vector
	std::vector< std::vector < int > > glims;			// Vector containing limits to be applied to each observable

	// Demix variables
	std::vector< std::vector< std::vector< double > > > kappas;
	std::vector< std::vector< std::vector< double > > > fracti;
	std::vector< std::vector< double > >  anchors;			// Vector containing anchor bins per observable
	std::vector< std::vector< double > >  reducib;			// Vector containing reducibility factors per observable
	std::vector< std::vector< double > >  reduerr;			// Vector containing reducibility factor errors per observable
	std::vector< std::vector< double > >  topfrac;			// Vector containing topic fractions per observable
	std::vector< std::vector< double > >  fracerr;			// Vector containing topic fractions errors per observable	


	public: 
		
		// Constructors and standard destructor
		demix();					
		demix ( const std::string & nfilem, const std::string & rfilem, const std::string & ofilem, const std::string & pfilem );
		demix ( std::vector< std::vector< TGraphErrors* > > input, std::vector< std::vector< double > > ilims, std::vector< std::vector< std::string > > names, const std::string & rfilem, const std::string & ofilem, const std::string & pfilem );
		demix ( std::vector< std::vector< TGraphErrors* > > input, std::vector< std::vector< int > > ilims, std::vector< std::vector< std::string > > names, const std::string & rfilem, std::vector < std::vector < std::string > > & ofilem, const std::string & pfilem );
		~demix();
		void readFile( std::string filen, int wfile );
		void readDat( std::string filen, bool inp );
		void readObs( std::string filen );
		void readPrm( std::string filen );

		// Base functions
		void 					getHists();								// Fill histogram objects
		TH1D* 					getHist( TFile* f, std::string name, bool full ); 
		void 					getPrunedHists();							// Kill tails of histogram and put them as overflow bins 
		std::vector< int > 			getPrunedHist( TH1D* hist );
		void 					getPrunedGrphs();							// Kill tails of histogram and put them as overflow bins 
		void 					getPrunedGrphs( std::vector< TGraphErrors* > G, int Obs );							// Kill tails of histogram and put them as overflow bins 
		void 					getHistsInLims(); 							// Get all histograms in given limits
		void 					getGrphsInLims ();
		void 					conHists2Grphs();							// Convert input histograms to graphs with errors
		void 					getTrimedGrphs();							// Get axis in a sensible range
		std::vector< std::vector< double > > 	getKappas( TH1D* S1, TH1D* S2 );					// Get reducibility (kappas) factors, their errors and anchor bins from two histograms 
		std::vector< std::vector< double > > 	getKappas( TGraphErrors* S1, TGraphErrors* S2, int Obs );			// Get reducibility (kappas) factors, their errors and anchor points from two graphs 
		std::vector< std::vector< double > > 	getFractions( std::vector< std::vector< double > > kappas );
		std::vector< TGraphErrors* > 		getResidues( TH1D* S1, TH1D* S2 );					// Get residue distributions from two histograms
		std::vector< TGraphErrors* > 		getResidues( TGraphErrors* S1, TGraphErrors* S2, int Obs );			// Get residue distributions from two graphs
		TGraphErrors* 				getAver( const std::vector< TGraphErrors* > graphs ); 			// Get uniformely distributed distribution in convex hull of given distributions
		TGraphErrors* 				getConv( const std::vector< TGraphErrors* > graphs ); 			// Get uniformely distributed distribution in convex hull of given distributions
		TGraphErrors* 				getConv( const std::vector< TGraphErrors* > graphs, vector< double > & rfracs ); 			// Get uniformely distributed distribution in convex hull of given distributions
		bool 					FaceTest( const std::vector< TGraphErrors* > R, int Obs ); 			// Test wether all reducibility factor between given distributions are zero. If not returns true
		TGraphErrors* 				TGraphErrorsScale( const TGraphErrors* S, const double & s );		// Function that scales TGraphErrors by some value (errors are scaled accordingly)
		TGraphErrors* 				TGraphErrorsSum( const TGraphErrors* S1, const TGraphErrors* S2 );	// Function that sums 
		int 					SquareDemix( const std::vector< TGraphErrors* > S, int Obs, std::vector< double > &fracs, int tol );
		std::vector< TGraphErrors* > 		SquareDemix( const std::vector< TGraphErrors* > S, int Obs );			// Squared demix algorithm with graphs so any number of samples and a number of topics equal to it
		std::vector< TGraphErrors* >		SquareDemix( const std::vector< TH1D* > S );				// Squared demix algorithm with histograms so 2 samples and 2 topics only (because of error propagation)
		void 					Demix();								// Demix algorithm for any number of samples and any number of topics lower than the former
		std::vector< std::vector< TGraphErrors* > >		getTopics();								// Returns the topics
		std::vector< std::vector< int > >		getGlims();								// Returns the global lims 
		std::vector< std::vector< std::vector< double > > >		getAfrcs();								// Returns the global lims 
		std::vector< std::vector < double > >				getGoodErrMeas();
		std::vector < double >				getGoodErrMeas( vector< TGraphErrors*> top, int iObs );
		std::vector< std::vector< std::vector< double > > >		getChi2T();								// Returns the global lims 
		void 					drawTopics( bool inp, bool ref, bool wait, std::string obs, std::string name ); 		// Drawing and printin results
		std::vector< std::string > 		parseString( std::string line, std::string deli);
		std::vector< int > 			getGlobXLims( const std::vector< std::vector< int > > & lims );
		void 					getHistinLims( TH1D* hist, std::vector< int > lims, bool keepflow );


		void testConv( int n, string A="A", string B="B", string C="C" );

		void testConv1( int n, string A="A", string B="B", string C="C" );

		void testConv2( int n , int tol, string A="A", string B="B" );

		void testconv3( int n, int iObs );
	
		void testconv4( int n, int iObs, int tol );

		std::vector< std::vector< double > > 	getSigmaDiff( std::vector< TGraphErrors* > top, std::vector< TGraphErrors* > ref, int iObs, bool r );
		std::vector< std::vector< std::vector< double > > > 	getSigmaDiff(bool r );





};

#endif
